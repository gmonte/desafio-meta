FROM node:10.16.3-alpine

ARG APP_PATH="/opt/desafio"

RUN mkdir -p ${APP_PATH}

WORKDIR ${APP_PATH}

RUN apk add --update tzdata

ENV TZ=America/Sao_Paulo

COPY package.json ./

COPY . .

RUN yarn -v

RUN yarn 

RUN yarn lerna bootstrap

EXPOSE 3001

RUN node -v

ENTRYPOINT [ "node" , "server/src/server/server.js" ]