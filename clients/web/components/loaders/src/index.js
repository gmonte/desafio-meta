import Linear from './components/Linear'
import Circular from './components/Circular'
import CircularFallback from './components/CircularFallback'

export { Linear as LinearLoader, Circular as CircularLoader, CircularFallback }
