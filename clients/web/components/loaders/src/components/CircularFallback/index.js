import React from 'react'
import CircularLoader from '../Circular'
import { useStyles } from './styles'

function CircularFallback(props) {
  const classes = useStyles()

  return (
    <div className={ classes.container }>
      <CircularLoader { ...props } />
    </div>
  )
}

export default CircularFallback
