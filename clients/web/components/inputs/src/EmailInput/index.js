import React from 'react'
import TextInput from '../TextInput'

const EmailInput = props => <TextInput { ...props } type="email" />

export default EmailInput
