import React from 'react'
import PropTypes from 'prop-types'
import isEmpty from 'lodash/isEmpty'
import isArray from 'lodash/isArray'
import get from 'lodash/get'
import first from 'lodash/first'
import MaskedInput from 'react-text-mask'
import moment from 'moment/moment'
import CalendarPicker from '@conheca-meta-clients/react-pickers/src/components/CalendarPicker'
import {
  dateFormat,
  dateTimeFormat,
  momentFriendlyDateFormat,
  momentBackDateFormat,
  momentFriendlyDateTimeFormat,
  momentBackDateTimeFormat,
  dateMask,
  dateTimeMask
} from '@conheca-meta-clients/react-pickers/src/utils'

import useStyles from './styles'

const InputMask = (props) => {
  const {
    inputRef,
    value,
    maxDate,
    minDate,
    writeText,
    openCalendar,
    disabled,
    enableTime,
    ...otherProps
    // inputRef
  } = props

  const classes = useStyles()

  let date = value

  if (isEmpty(value)) {
    date = null
  } else {
    // test if value is the backend format for transform to friendly format
    const test = moment(value, enableTime ? momentBackDateTimeFormat : momentBackDateFormat, true)
    if (test.isValid()) {
      date = test.format(enableTime ? momentFriendlyDateTimeFormat : momentFriendlyDateFormat)
    }
  }

  const onDataChange = (customDate, config) => {
    const { onChange } = props

    if (isArray(customDate)) {
      customDate = first(customDate)
    }

    const manual = get(config, 'manual', false)
    const test = moment(
      customDate,
      // eslint-disable-next-line no-nested-ternary
      manual
        ? (
          enableTime
            ? momentFriendlyDateTimeFormat
            : momentFriendlyDateFormat
        ) : undefined,
      true
    )

    if (test.isValid()) {
      customDate = test.format(enableTime ? momentBackDateTimeFormat : momentBackDateFormat)
    }

    onChange({
      target: {
        value: customDate
      }
    })
  }

  return (
    <CalendarPicker
      value={ date }
      onChange={ onDataChange }
      options={ {
        mode: 'single',
        wrap: true,
        allowInput: !enableTime && writeText,
        clickOpens: openCalendar,
        dateFormat: enableTime ? dateTimeFormat : dateFormat,
        maxDate,
        minDate,
        enableTime
      } }
    >
      <MaskedInput
        // ref={ inputRef }
        data-input
        mask={ enableTime ? dateTimeMask : dateMask }
        guide={ false }
        autoComplete="off"
        value={ date }
        className={ [
          classes.input,
          isEmpty(date) ? classes.inputEmpty : null,
          disabled ? classes.inputDisabled : null
        ].join(' ') }
        onChange={ ({ target: { value: newValue } }) => onDataChange(newValue, { manual: true }) }
        { ...otherProps }
      />
    </CalendarPicker>
  )
}

InputMask.propTypes = {
  inputRef: PropTypes.func.isRequired,
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.shape()]).isRequired,
  onChange: PropTypes.func.isRequired,
  writeText: PropTypes.bool.isRequired,
  openCalendar: PropTypes.bool.isRequired,
  maxDate: PropTypes.string.isRequired,
  minDate: PropTypes.string.isRequired,
  enableTime: PropTypes.bool.isRequired,
  disabled: PropTypes.bool
}

InputMask.defaultProps = {
  disabled: false
}

export default InputMask
