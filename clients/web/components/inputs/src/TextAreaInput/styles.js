import { makeStyles } from '@material-ui/core/styles'

const useStyles = makeStyles(() => ({
  textField: {
    marginLeft: 0,
    marginRight: 0,
    marginTop: 5,
    marginBottom: 10
  }
}))

export default useStyles
