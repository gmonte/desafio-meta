import React from 'react'
import TextInput from '../TextInput'

const TextAreaInput = props => <TextInput rows={ 3 } { ...props } multiline />

export default TextAreaInput
