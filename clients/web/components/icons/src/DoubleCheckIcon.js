import React from 'react'
import { mdiCheckAll } from '@mdi/js'
import Icon from '@mdi/react'

const DoubleCheckIcon = props => <Icon size={ 1 } path={ mdiCheckAll } { ...props } />

export default DoubleCheckIcon
