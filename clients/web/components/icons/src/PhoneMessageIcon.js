import React from 'react'
import { mdiPhoneMessage } from '@mdi/js'
import Icon from '@mdi/react'

const PhoneMessageIcon = props => <Icon size={ 1 } path={ mdiPhoneMessage } { ...props } />

export default PhoneMessageIcon
