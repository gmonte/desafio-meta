import { Colors } from '@conheca-meta-clients/styles'

export default () => ({
  searchField: {
    marginLeft: 10,
    marginRight: 10
  },
  stripedRows: {
    '& > tr': {
      '&:nth-child(odd)': {
        backgroundColor: Colors.grey[4]
      },
      '&:nth-child(even)': {
        backgroundColor: Colors.grey[3]
      }
    }
  }
})
