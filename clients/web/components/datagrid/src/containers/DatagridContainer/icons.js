import AddIcon from '@conheca-meta-clients/react-icons/src/AddIcon'
import CheckIcon from '@conheca-meta-clients/react-icons/src/CheckIcon'
import CloseIcon from '@conheca-meta-clients/react-icons/src/CloseIcon'
import DeleteIcon from '@conheca-meta-clients/react-icons/src/DeleteIcon'
import DetailPanelIcon from '@conheca-meta-clients/react-icons/src/DetailPanelIcon'
import EditIcon from '@conheca-meta-clients/react-icons/src/EditIcon'
import ExportIcon from '@conheca-meta-clients/react-icons/src/ExportIcon'
import FilterIcon from '@conheca-meta-clients/react-icons/src/FilterIcon'
import FirstPageIcon from '@conheca-meta-clients/react-icons/src/FirstPageIcon'
import SortArrowIcon from '@conheca-meta-clients/react-icons/src/SortArrowIcon'
import LastPageIcon from '@conheca-meta-clients/react-icons/src/LastPageIcon'
import NextPageIcon from '@conheca-meta-clients/react-icons/src/NextPageIcon'
import PreviousPageIcon from '@conheca-meta-clients/react-icons/src/PreviousPageIcon'
import BroomIcon from '@conheca-meta-clients/react-icons/src/BroomIcon'
import SearchIcon from '@conheca-meta-clients/react-icons/src/SearchIcon'
import RemoveIcon from '@conheca-meta-clients/react-icons/src/RemoveIcon'
import ViewColumnIcon from '@conheca-meta-clients/react-icons/src/ViewColumnIcon'

export default {
  Add: AddIcon,
  Check: CheckIcon,
  Clear: CloseIcon,
  Delete: DeleteIcon,
  DetailPanel: DetailPanelIcon,
  Edit: EditIcon,
  Export: ExportIcon,
  Filter: FilterIcon,
  FirstPage: FirstPageIcon,
  SortArrow: SortArrowIcon,
  LastPage: LastPageIcon,
  NextPage: NextPageIcon,
  PreviousPage: PreviousPageIcon,
  ResetSearch: BroomIcon,
  Search: SearchIcon,
  ThirdStateCheck: RemoveIcon,
  ViewColumn: ViewColumnIcon
}
