import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import flow from 'lodash/fp/flow'
import reduce from 'lodash/reduce'
import isEmpty from 'lodash/isEmpty'
import isEqual from 'lodash/isEqual'
import isFunction from 'lodash/isFunction'
import map from 'lodash/map'
import snakeCase from 'lodash/snakeCase'
import get from 'lodash/get'
import { withStyles } from '@material-ui/core/styles'
import { injectI18n } from '@meta-react/i18n'
import MaterialTable from 'material-table'

import options from './options'
import icons from './icons'
import datagridI18 from './i18n'

import styles from './styles'

class DatagridContainer extends PureComponent {
  configs = undefined

  subscriber = undefined

  constructor(props) {
    super(props)

    const {
      intl: { formatMessage }
    } = props

    this.i18n = datagridI18(formatMessage)
  }

  asyncData = async (query) => {
    const {
      data, pageSize, params: customParams
    } = this.props

    try {
      const filters = get(query, 'filters')
      const searchTerm = get(query, 'search')
      const perPage = get(query, 'pageSize', pageSize)
      const orderBy = get(query, 'orderBy.field')
      const orderDirection = get(query, 'orderDirection')
      let page = get(query, 'page', 0)

      const formattedFilters = reduce(
        filters,
        (acc, filter) => ({ ...acc, [get(filter, 'column.field')]: get(filter, 'value') }),
        {}
      )

      const currentConfigs = {
        perPage,
        searchTerm,
        formattedFilters,
        orderBy,
        orderDirection
      }

      if (!isEqual(currentConfigs, this.configs)) {
        page = 0 // if has changes, return to first page
      }
      this.configs = { ...currentConfigs }

      let params = {
        ...customParams,
        page: page + 1,
        perPage
      }
      if (!isEmpty(orderBy)) {
        params = {
          ...params,
          orderBy: snakeCase(orderBy),
          orderDirection
        }
      }
      if (!isEmpty(searchTerm)) {
        params = {
          ...params,
          searchTerm
        }
      }
      if (!isEmpty(formattedFilters)) {
        params = {
          ...params,
          operator: 'and',
          ...formattedFilters
        }
      }

      const {
        data: { data: datagridData }
      } = await data({
        data: params
      })

      const { results, resultsFound } = datagridData

      return {
        data: results,
        page,
        totalCount: resultsFound
      }
    } catch (e) {
      throw e
    }
  }

  render() {
    const {
      classes,
      data,
      options: propOptions,
      noShadow,
      title,
      onAddClick,
      addTooltip,
      editCondition,
      deleteCondition,
      onEditClick,
      onDeleteClick,
      onExportClick,
      pageSize,
      loading,
      disabled,
      disabledRow,
      intl: { formatMessage },
      emptyMessage,
      ...otherProps
    } = this.props

    let { actions, columns } = this.props

    columns = map(columns, (column) => {
      const { cellStyle = {} } = column

      let columnStyle = cellStyle
      if (!isFunction(cellStyle)) {
        columnStyle = () => cellStyle
      }

      return {
        ...column,
        cellStyle: (_, rowData) => {
          let rowStyle = {}
          if (disabledRow(rowData)) {
            rowStyle = {
              opacity: 0.5
            }
          }
          return {
            ...columnStyle,
            ...rowStyle
          }
        }
      }
    })

    const customOptions = {
      ...options,
      ...propOptions,
      pageSize,
      showTitle: !isEmpty(title),
      searchFieldProps: {
        type: 'search',
        classes: {
          root: classes.searchField
        },
        InputProps: {
          endAdornment: null
        }
      },
      tableBodyProps: {
        className: classes.stripedRows
      }
    }

    let style = {}
    if (noShadow) {
      style = {
        ...style,
        boxShadow: 'none'
      }
    }

    if (onExportClick) {
      const ExportIcon = icons.Export
      actions = [
        {
          icon: () => <ExportIcon />,
          tooltip: formatMessage({ id: 'datagrid-toolbar-export-title' }),
          onClick: onExportClick,
          isFreeAction: true,
          disabled: loading || disabled
        },
        ...actions
      ]
    }

    if (onAddClick) {
      const AddIcon = icons.Add
      actions = [
        {
          icon: () => <AddIcon />,
          tooltip: addTooltip,
          onClick: onAddClick,
          isFreeAction: true,
          fab: true,
          disabled: loading || disabled,
          iconButtonProps: {
            color: 'primary',
            size: 'small'
          }
        },
        ...actions
      ]
    }

    if (onDeleteClick) {
      const DeleteIcon = icons.Delete
      actions = [
        rowData => ({
          icon: () => <DeleteIcon />,
          tooltip: formatMessage({ id: 'do-delete' }),
          onClick: onDeleteClick,
          disabled: loading || disabled || !deleteCondition(rowData)
        }),
        ...actions
      ]
    }

    if (onEditClick) {
      const EditIcon = icons.Edit
      actions = [
        rowData => ({
          icon: () => <EditIcon />,
          tooltip: formatMessage({ id: 'edit' }),
          onClick: onEditClick,
          disabled: loading || disabled || !editCondition(rowData)
        }),
        ...actions
      ]
    }

    let realData = data
    let customProps = {}
    if (isFunction(data)) {
      realData = this.asyncData
    } else {
      customProps = {
        ...customProps,
        isLoading: loading
      }
    }

    const i18n = {
      ...(this.i18n || {}),
      body: {
        ...get(this.i18n, 'body', {}),
        emptyDataSourceMessage: emptyMessage || get(this.i18n, 'body.emptyDataSourceMessage')
      }
    }

    return (
      <MaterialTable
        { ...otherProps }
        { ...customProps }
        icons={ icons }
        options={ customOptions }
        columns={ columns }
        data={ realData }
        localization={ i18n }
        style={ style }
        title={ title }
        actions={ actions }
      />
    )
  }
}

DatagridContainer.propTypes = {
  /** - */
  classes: PropTypes.shape().isRequired,
  /** - */
  intl: PropTypes.shape().isRequired,
  /** columns by material-table */
  columns: PropTypes.array.isRequired,
  /** actions by material-table */
  actions: PropTypes.array,
  /** data by material-table */
  data: PropTypes.oneOfType([PropTypes.array, PropTypes.func, PropTypes.string]),
  /** options by material-table */
  options: PropTypes.shape(),
  /** params to inject into request */
  params: PropTypes.shape(),
  /** pageSize by material-table */
  pageSize: PropTypes.number,
  /** remove shadow of the datagrid container */
  noShadow: PropTypes.bool,
  /** title by material-table */
  title: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.element
  ]),
  /** Enable de Add button, and set the callback called when this button is clicked */
  onAddClick: PropTypes.func,
  /** tooltip for add button */
  addTooltip: PropTypes.string,
  /** tooltip for edit button */
  editCondition: PropTypes.func,
  /** function that enable the Delete button for each row */
  deleteCondition: PropTypes.func,
  /** Enable de Edit button, and set the callback called when this button is clicked */
  onEditClick: PropTypes.func,
  /** Enable de Delete button, and set the callback called when this button is clicked */
  onDeleteClick: PropTypes.func,
  /** Enable de Export button, and set the callback called when this button is clicked */
  onExportClick: PropTypes.func,
  /** datagrid is loading */
  loading: PropTypes.bool,
  /** disable all actions */
  disabled: PropTypes.bool,
  /** customize row when row was softdeleted */
  disabledRow: PropTypes.func,
  /** customize row when row was softdeleted */
  emptyMessage: PropTypes.string
}

DatagridContainer.defaultProps = {
  data: [],
  options: {},
  params: {},
  noShadow: false,
  actions: [],
  pageSize: 10,
  title: '',
  onAddClick: null,
  addTooltip: '',
  editCondition: () => true,
  deleteCondition: () => true,
  onEditClick: null,
  onDeleteClick: null,
  onExportClick: null,
  loading: false,
  disabled: false,
  disabledRow: () => false,
  emptyMessage: null
}

export default flow(
  withStyles(styles),
  injectI18n
)(DatagridContainer)

export { DatagridContainer as DatagridProps }
