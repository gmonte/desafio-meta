export default formatMessage => ({
  body: {
    addTooltip: formatMessage({ id: 'datagrid body add tooltip' }),
    deleteTooltip: formatMessage({ id: 'datagrid body delete tooltip' }),
    editRow: {
      deleteText: formatMessage({ id: 'datagrid body edit row delete text' }),
      cancelTooltip: formatMessage({ id: 'datagrid body edit row cancel tooltip' }),
      saveTooltip: formatMessage({ id: 'datagrid body edit row save tooltip' })
    },
    editTooltip: formatMessage({ id: 'datagrid body edit tooltip' }),
    emptyDataSourceMessage: formatMessage({ id: 'datagrid body empty data source message' }),
    filterRow: {
      filterTooltip: formatMessage({ id: 'datagrid body filter row filter tooltip' })
    }
  },
  grouping: {
    groupedBy: formatMessage({ id: 'datagrid grouping grouped by' }),
    placeholder: formatMessage({ id: 'datagrid grouping placeholder' })
  },
  header: {
    actions: formatMessage({ id: 'datagrid header actions' })
  },
  pagination: {
    labelDisplayedRows: formatMessage(
      {
        id: 'datagrid pagination label displayed rows'
      },
      {
        from: '{from}',
        to: '{to}',
        count: '{count}'
      }
    ),
    labelRowsSelect: formatMessage({ id: 'datagrid pagination label rows select' }),
    labelRowsPerPage: formatMessage({ id: 'datagrid pagination label rows per page' }),
    firstAriaLabel: formatMessage({ id: 'datagrid pagination first aria label' }),
    firstTooltip: formatMessage({ id: 'datagrid pagination first tooltip' }),
    previousAriaLabel: formatMessage({ id: 'datagrid pagination previous aria label' }),
    previousTooltip: formatMessage({ id: 'datagrid pagination previous tooltip' }),
    nextAriaLabel: formatMessage({ id: 'datagrid pagination next aria label' }),
    nextTooltip: formatMessage({ id: 'datagrid pagination next tooltip' }),
    lastAriaLabel: formatMessage({ id: 'datagrid pagination last aria label' }),
    lastTooltip: formatMessage({ id: 'datagrid pagination last tooltip' })
  },
  toolbar: {
    addRemoveColumns: formatMessage({ id: 'datagrid toolbar add remove columns' }),
    nRowsSelected: formatMessage({ id: 'datagrid toolbar n rows selected' }, { '0': '{0}' }),
    showColumnsTitle: formatMessage({ id: 'datagrid toolbar show columns title' }),
    showColumnsAriaLabel: formatMessage({ id: 'datagrid toolbar show columns aria label' }),
    exportTitle: formatMessage({ id: 'datagrid toolbar export title' }),
    exportAriaLabel: formatMessage({ id: 'datagrid toolbar export aria label' }),
    exportName: formatMessage({ id: 'datagrid toolbar export name' }),
    searchTooltip: formatMessage({ id: 'datagrid toolbar search tooltip' }),
    searchPlaceholder: formatMessage({ id: 'datagrid toolbar search placeholder' })
  }
})
