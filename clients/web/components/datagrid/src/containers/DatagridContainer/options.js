export default {
  // loadingType: 'linear',
  selection: false,
  columnsButton: true,
  grouping: true,
  filtering: true,
  debounceInterval: 500,
  pageSizeOptions: [4, 10, 20, 50, 100],
  initialPage: 0,
  actionsColumnIndex: -1,
  headerStyle: {
    zIndex: 11
  },
  notShowEmptyRows: true
}
