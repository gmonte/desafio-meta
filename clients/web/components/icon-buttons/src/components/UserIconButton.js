import React, { useContext } from 'react'
import UserIcon from '@conheca-meta-clients/react-icons/src/UserIcon'
import { I18nContext } from '@meta-react/i18n'
import IconButton from '../index'

const UserIconButton = (props) => {
  const { formatMessage } = useContext(I18nContext)

  return (
    <IconButton tooltip={ formatMessage({ id: 'user' }) } { ...props }>
      <UserIcon />
    </IconButton>
  )
}

export default UserIconButton
