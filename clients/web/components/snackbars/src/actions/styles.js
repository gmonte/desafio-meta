import { Colors } from '@conheca-meta-clients/styles'

export default () => ({
  variantWarning: {
    color: Colors.text
  }
})
