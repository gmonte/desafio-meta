import { createContext } from 'react'
import uniqBy from 'lodash/uniqBy'
import filter from 'lodash/filter'
import map from 'lodash/map'

export const CREATE_DIALOG = 'CREATE_DIALOG'
export const REMOVE_DIALOG = 'REMOVE_DIALOG'
export const DESTROY_DIALOG = 'DESTROY_DIALOG'
export const RESET_DIALOGS = 'RESET_DIALOGS'

export const DialogContext = createContext()

export const dialogReducer = (dialogs = [], action = {}) => {
  switch (action.type) {
    case CREATE_DIALOG:
      return uniqBy([{ ...action.dialog, open: true }, ...dialogs], 'id')

    case REMOVE_DIALOG:
      return map(dialogs, dialog => ({
        ...dialog,
        open: dialog.id === action.dialog.id ? false : dialog.open
      }))

    case DESTROY_DIALOG:
      return filter(dialogs, dialog => dialog.id !== action.dialog.id)

    case RESET_DIALOGS:
      return []

    default:
      return dialogs
  }
}
