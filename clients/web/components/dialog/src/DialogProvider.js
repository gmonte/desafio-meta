import React, { useReducer } from 'react'
import PropTypes from 'prop-types'
import map from 'lodash/map'
import {
  CREATE_DIALOG,
  DESTROY_DIALOG,
  REMOVE_DIALOG,
  RESET_DIALOGS,
  DialogContext,
  dialogReducer
} from './dialogReducer'

const DialogProvider = ({ children = null }) => {
  const [dialogs, dispatch] = useReducer(dialogReducer, [])

  const createDialog = dialog => dispatch({ type: CREATE_DIALOG, dialog })
  const destroyDialog = dialog => dispatch({ type: DESTROY_DIALOG, dialog })
  const removeDialog = (dialog) => {
    dispatch({ type: REMOVE_DIALOG, dialog })
    setTimeout(() => destroyDialog(dialog), 300)
  }
  const resetDialogs = () => dispatch({ type: RESET_DIALOGS })

  const state = {
    createDialog: dialog => createDialog(dialog),
    removeDialog: dialog => removeDialog(dialog),
    destroyDialog: dialog => destroyDialog(dialog),
    resetDialogs,
    dialogs
  }

  const renderDialog = (dialog) => {
    const {
      id,
      Modal: DialogComponent,
      open,
      props = {}
    } = dialog

    return (
      <DialogComponent
        key={ id }
        id={ id }
        open={ open }
        handleClose={ () => removeDialog({ id }) }
        { ...props }
      />
    )
  }

  return (
    <DialogContext.Provider value={ state }>
      {map(dialogs, renderDialog)}

      {children}
    </DialogContext.Provider>
  )
}

DialogProvider.propTypes = {
  children: PropTypes.any.isRequired
}

export default DialogProvider
