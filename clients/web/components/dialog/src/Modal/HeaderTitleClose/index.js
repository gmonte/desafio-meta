import React from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'
import DialogTitle from '@material-ui/core/DialogTitle/DialogTitle'
import CloseIconButton from '@conheca-meta-clients/react-icon-buttons/src/components/CloseIconButton'

import styles from './styles'

const HeaderTitleClose = (props) => {
  const {
    classes, title, disableTypography, titleClass, onClose, escape, containerClass
  } = props

  return (
    <div className={ [classes.header, containerClass].join(' ') }>
      <DialogTitle className={ [classes.title, titleClass].join(' ') } disableTypography={ disableTypography }>
        {title}
      </DialogTitle>
      {escape ? <CloseIconButton marginHorizontal marginVertical onClick={ onClose } /> : null}
    </div>
  )
}

HeaderTitleClose.propTypes = {
  classes: PropTypes.object.isRequired,
  escape: PropTypes.bool.isRequired,
  disableTypography: PropTypes.bool,
  titleClass: PropTypes.string,
  title: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.element
  ]),
  onClose: PropTypes.func,
  containerClass: PropTypes.string
}

HeaderTitleClose.defaultProps = {
  disableTypography: false,
  title: null,
  titleClass: null,
  onClose: () => {},
  containerClass: null
}

export default withStyles(styles)(HeaderTitleClose)
