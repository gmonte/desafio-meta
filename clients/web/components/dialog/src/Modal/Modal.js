import React, { useContext } from 'react'
import PropTypes from 'prop-types'
import { withStyles, useTheme } from '@material-ui/core/styles'
import useMediaQuery from '@material-ui/core/useMediaQuery'
import Dialog from '@material-ui/core/Dialog'
import DialogContent from '@material-ui/core/DialogContent'
import { LinearLoader } from '@conheca-meta-clients/react-loaders'
import HeaderTitleClose from './HeaderTitleClose'
import { DialogContext } from '../dialogReducer'

import styles from './styles'

const Modal = (props) => {
  const {
    classes,
    contentText,
    children,
    id,
    open,
    escape,
    loading,
    title,
    onClose,
    FooterComponent,
    position,
    headerProps,
    ...otherProps
  } = props

  const theme = useTheme()

  const fullScreen = useMediaQuery(theme.breakpoints.down('sm'))

  const { removeDialog } = useContext(DialogContext)

  const close = () => {
    removeDialog({ id })
    onClose()
  }

  const componentsProps = {
    onClose: close,
    title,
    escape
  }

  return (
    <Dialog
      id={ id }
      open={ open }
      onClose={ close }
      disableEscapeKeyDown={ !escape }
      disableBackdropClick={ !escape }
      disableRestoreFocus
      disableEnforceFocus
      fullScreen={ fullScreen }
      classes={ {
        scrollPaper: position === 'top' ? classes.dialogTop : undefined
      } }
      { ...otherProps }
    >
      <HeaderTitleClose { ...componentsProps } { ...headerProps } />

      <DialogContent>
        <div className={ classes.contentText }>{contentText}</div>
        {children}
      </DialogContent>

      <FooterComponent { ...componentsProps } />

      <LinearLoader visible={ loading } loading={ loading } />
    </Dialog>
  )
}

Modal.propTypes = {
  classes: PropTypes.object.isRequired,
  id: PropTypes.string.isRequired,
  open: PropTypes.bool.isRequired,
  onClose: PropTypes.func,
  title: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.element
  ]),
  FooterComponent: PropTypes.func,
  contentText: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
  children: PropTypes.element,
  escape: PropTypes.bool,
  loading: PropTypes.bool,
  position: PropTypes.oneOf(['middle', 'top']),
  headerProps: PropTypes.object
}

Modal.defaultProps = {
  title: null,
  FooterComponent: () => null,
  contentText: null,
  children: null,
  escape: true,
  loading: false,
  onClose: () => {},
  position: 'middle',
  headerProps: {}
}

export default withStyles(styles)(Modal)
