export default theme => ({
  contentText: {
    fontFamily: theme.typography.fontFamily,
    // color: Colors.muted,
    // fontSize: Fonts.fontSize.M,
    '& p': {
      marginBottom: 5
    }
  },
  dialogTop: {
    alignItems: 'flex-start'
  }
})
