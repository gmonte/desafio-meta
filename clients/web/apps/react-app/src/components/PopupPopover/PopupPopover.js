import React from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'
import Popover from '@material-ui/core/Popover'
import { usePopupState, bindTrigger, bindPopover } from 'material-ui-popup-state/hooks'

const styles = theme => ({
  typography: {
    margin: theme.spacing(2)
  }
})

const PopupPopover = (props) => {
  const {
    classes, parentComponent: Btn, component: Popup, parentProps, componentProps, ...otherProps
  } = props

  const popupState = usePopupState({
    variant: 'popover',
    popupId: 'demoPopover'
  })

  const btnProps = bindTrigger(popupState)
  const popoverProps = bindPopover(popupState)

  return (
    <div>
      <Btn { ...{ ...parentProps, ...btnProps } } />
      <Popover { ...{ ...otherProps, ...popoverProps } }>
        <Popup { ...componentProps } onClosePopup={ popoverProps.onClose } />
      </Popover>
    </div>
  )
}

PopupPopover.propTypes = {
  classes: PropTypes.shape().isRequired,
  parentComponent: PropTypes.oneOfType([PropTypes.shape(), PropTypes.func]).isRequired,
  component: PropTypes.oneOfType([PropTypes.shape(), PropTypes.func]).isRequired,
  parentProps: PropTypes.shape(),
  componentProps: PropTypes.shape(),
  anchorOrigin: PropTypes.shape(),
  transformOrigin: PropTypes.shape()
}

PopupPopover.defaultProps = {
  parentProps: {},
  componentProps: {},
  anchorOrigin: {
    vertical: 'bottom',
    horizontal: 'center'
  },
  transformOrigin: {
    vertical: 'top',
    horizontal: 'center'
  }
}

export default withStyles(styles)(PopupPopover)
