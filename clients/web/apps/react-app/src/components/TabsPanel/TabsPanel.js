/* eslint-disable react/require-default-props */
import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types'
import { makeStyles, useTheme } from '@material-ui/core/styles'
import AppBar from '@material-ui/core/AppBar'
import Tabs from '@material-ui/core/Tabs'
import Tab from '@material-ui/core/Tab'
import Typography from '@material-ui/core/Typography'
import Box from '@material-ui/core/Box'
import map from 'lodash/map'
import hexToRgba from 'hex-to-rgba'
import { Fonts, Colors } from '@conheca-meta-clients/styles'

function TabPanel(props) {
  const {
    children, value, index, ...other
  } = props

  return (
    <Typography
      component="div"
      // role="tabpanel"
      hidden={ value !== index }
      id={ `full-width-tabpanel-${ index }` }
      aria-labelledby={ `full-width-tab-${ index }` }
      { ...other }
    >
      <Box p={ 2 }>{children}</Box>
    </Typography>
  )
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
}

function a11yProps(index) {
  return {
    id: `full-width-tab-${ index }`,
    'aria-controls': `full-width-tabpanel-${ index }`,
  }
}

const useStyles = makeStyles(theme => ({
  root: {
    backgroundColor: theme.palette.background.paper,
    width: '100%',
  },
  textColorSecondaryPrimary: {
    color: hexToRgba(Colors.primary, 0.54),
    fontFamily: Fonts.fontFamilyLobster,
    fontSize: Fonts.fontSize.L
  },
  textColorSecondarySecondary: {
    color: hexToRgba(Colors.secondary, 0.54),
  },
  textColorSecondary: {
    textTransform: 'none',
    fontFamily: Fonts.fontFamilyLobster,
    fontSize: Fonts.fontSize.XL
  }
}))

// eslint-disable-next-line react/prop-types
export default function FullWidthTabs({ tabs, defaultTabType }) {
  const classes = useStyles()
  const theme = useTheme()

  const getDefaultTabTypeAsIndex = () => {
    if (defaultTabType === 'vemPraMeta') {
      return 0
    }

    if (defaultTabType === 'conhecaMeta') {
      return 1
    }

    return 0
  }

  const [value, setValue] = useState(getDefaultTabTypeAsIndex())

  useEffect(() => {
    if (defaultTabType) {
      const newValue = getDefaultTabTypeAsIndex()
      if (newValue !== value) {
        setValue(newValue)
      }
    }
    // eslint-disable-next-line
  }, [defaultTabType])

  const handleChange = (event, newValue) => {
    setValue(newValue)
  }

  return (
    <div className={ classes.root }>
      <AppBar position="static" color={ value === 0 ? 'secondary' : 'primary' }>
        <Tabs
          value={ value }
          onChange={ handleChange }
          indicatorColor={ value === 0 ? 'primary' : 'secondary' }
          textColor={ value === 0 ? 'primary' : 'secondary' }
          variant="fullWidth"
        >
          {
            map(tabs, ({ title }, index) => (
              <Tab
                key={ title }
                label={ title }
                classes={ {
                  [value === 0 ? 'textColorPrimary' : 'textColorSecondary']: [
                    classes.textColorSecondary,
                    value === 0
                      ? classes.textColorSecondaryPrimary
                      : classes.textColorSecondarySecondary
                  ].join(' ')
                } }
                { ...a11yProps(index) }
              />
            ))
          }
        </Tabs>
      </AppBar>
      {
        map(tabs, ({ title, TabContent }, index) => (
          <TabPanel key={ title } value={ value } index={ index } dir={ theme.direction }>
            <TabContent />
          </TabPanel>
        ))
      }
    </div>
  )
}
