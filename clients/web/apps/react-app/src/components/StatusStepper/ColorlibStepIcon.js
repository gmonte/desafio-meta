import React from 'react'
import PropTypes from 'prop-types'
import { makeStyles } from '@material-ui/core/styles'
import clsx from 'clsx'
import CheckIcon from '@conheca-meta-clients/react-icons/src/CheckIcon'
import DoubleCheckIcon from '@conheca-meta-clients/react-icons/src/DoubleCheckIcon'
import BalloonCheckIcon from '@conheca-meta-clients/react-icons/src/BalloonCheckIcon'
import VisitedIcon from '@conheca-meta-clients/react-icons/src/VisitedIcon'
import TrophyIcon from '@conheca-meta-clients/react-icons/src/TrophyIcon'
import GroupAddIcon from '@conheca-meta-clients/react-icons/src/GroupAddIcon'
import CloseIcon from '@conheca-meta-clients/react-icons/src/CloseIcon'
import { stepperColors } from './ColorlibConnector'

const useColorlibStepIconStyles = makeStyles({
  root: {
    backgroundColor: '#ccc',
    zIndex: 1,
    color: '#fff',
    width: 50,
    height: 50,
    display: 'flex',
    borderRadius: '50%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  active: {
    backgroundImage:
      `linear-gradient( 136deg, ${ stepperColors.one } 0%, ${ stepperColors.two } 50%, ${ stepperColors.three } 100%)`,
    boxShadow: '0 4px 10px 0 rgba(0,0,0,.25)',
  },
  activeRejected: {
    backgroundColor: 'red',
    boxShadow: '0 4px 10px 0 rgba(0,0,0,.25)',
  },
  completed: {
    backgroundImage:
      `linear-gradient( 136deg, ${ stepperColors.one } 0%, ${ stepperColors.two } 50%, ${ stepperColors.three } 100%)`,
  },
})

function ColorlibStepIcon(props) {
  const classes = useColorlibStepIconStyles()
  const { active, completed, inviteStatus } = props

  const icons = {
    1: <CheckIcon />,
    2: <DoubleCheckIcon color="#fff" />,
    3: inviteStatus === 'REJECTED' ? <CloseIcon /> : <BalloonCheckIcon />,
    4: <VisitedIcon />,
    5: <GroupAddIcon />,
    6: <TrophyIcon />
  }

  return (
    <div
      className={ clsx(classes.root, {
        [classes.active]: active && inviteStatus !== 'REJECTED',
        [classes.activeRejected]: active && inviteStatus === 'REJECTED',
        [classes.completed]: completed,
      }) }
    >
      {icons[String(props.icon)]}
    </div>
  )
}

ColorlibStepIcon.propTypes = {
  active: PropTypes.bool,
  completed: PropTypes.bool,
  icon: PropTypes.node,
  inviteStatus: PropTypes.string.isRequired
}

ColorlibStepIcon.defaultProps = {
  active: false,
  completed: false,
  icon: null
}

export default ColorlibStepIcon
