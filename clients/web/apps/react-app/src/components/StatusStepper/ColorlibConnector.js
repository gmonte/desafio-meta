import { withStyles } from '@material-ui/core/styles'
import StepConnector from '@material-ui/core/StepConnector'
import { Colors } from '@conheca-meta-clients/styles'

export const stepperColors = {
  one: Colors.secondary,
  two: Colors.blue[1],
  three: Colors.primary
}

const ColorlibConnector = withStyles({
  alternativeLabel: {
    top: 22,
  },
  active: {
    '& $line': {
      backgroundImage:
        `linear-gradient( 95deg, ${ stepperColors.one } 0%,${ stepperColors.two } 50%,${ stepperColors.three } 100%)`,
    },
  },
  completed: {
    '& $line': {
      backgroundImage:
        `linear-gradient( 95deg, ${ stepperColors.one } 0%,${ stepperColors.two } 50%,${ stepperColors.three } 100%)`,
    },
  },
  line: {
    height: 3,
    border: 0,
    backgroundColor: '#eaeaf0',
    borderRadius: 1,
  },
})(StepConnector)

export default ColorlibConnector
