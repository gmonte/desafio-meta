import React, { useContext } from 'react'
import PropTypes from 'prop-types'
import { makeStyles } from '@material-ui/core/styles'
import Stepper from '@material-ui/core/Stepper'
import Step from '@material-ui/core/Step'
import StepLabel from '@material-ui/core/StepLabel'
import { DialogContext } from '@conheca-meta-clients/react-dialog'
import ColorlibStepIcon from './ColorlibStepIcon'
import ColorlibConnector from './ColorlibConnector'
import VisitedConfirmationModal from '../../modals/VisitedConfirmationModal'

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%'
  },
  button: {
    marginRight: theme.spacing(1)
  },
  instructions: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(1)
  }
}))

const StatusStepper = (props) => {
  const { steps, activeStep, invite } = props

  const { status, type } = invite

  const { createDialog } = useContext(DialogContext)

  const classes = useStyles()

  return (
    <div className={ classes.root }>
      <Stepper alternativeLabel activeStep={ activeStep } connector={ <ColorlibConnector /> }>
        {steps.map((label, index) => (
          <Step
            key={ label }
            onClick={ (e) => {
              if (status === 'ACCEPTED' && index === 3) {
                e.stopPropagation()
                e.preventDefault()
                createDialog({
                  id: 'visited-confirmation',
                  Modal: VisitedConfirmationModal,
                  props: {
                    invite
                  }
                })
              }
            } }
            style={ {
              cursor: status === 'ACCEPTED' && index === 3 ? 'pointer' : 'normal'
            } }
          >
            <StepLabel StepIconComponent={ stepProps => <ColorlibStepIcon inviteStatus={ status } { ...stepProps } /> }>
              {label}
              {status === 'ACCEPTED' && index === 3 && (
                <>
                  <br />
                  <b>{type === 'conhecaMeta' ? 'Confirmar visita' : 'Confirmar entrevista'}</b>
                </>
              )}
            </StepLabel>
          </Step>
        ))}
      </Stepper>
    </div>
  )
}

StatusStepper.propTypes = {
  steps: PropTypes.array,
  activeStep: PropTypes.number,
  invite: PropTypes.object.isRequired
}

StatusStepper.defaultProps = {
  steps: [],
  activeStep: 0
}

export default StatusStepper
