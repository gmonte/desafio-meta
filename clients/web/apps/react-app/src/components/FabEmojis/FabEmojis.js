import React from 'react'
import PropTypes from 'prop-types'
import { Emoji } from 'emoji-mart'
import map from 'lodash/map'
import Typography from '@material-ui/core/Typography'
import Fab from '@material-ui/core/Fab'

import useStyles from './styles'

const FabEmojis = (props) => {
  const {
    text,
    emojis,
    onClick,
    disabled
  } = props

  const classes = useStyles()

  return (
    <Fab
      variant="extended"
      color="secondary"
      onClick={ onClick }
      className={ classes.button }
      disabled={ disabled }
    >
      <Typography variant="h5" component="h1" color="primary" className={ classes.text }>
        {text}
      </Typography>
      <div className={ classes.emojisContainer }>
        {
          map(emojis, emoji => <Emoji key={ emoji } emoji={ { id: emoji } } size={ 35 } />)
        }
      </div>
    </Fab>
  )
}

FabEmojis.propTypes = {
  text: PropTypes.string.isRequired,
  emojis: PropTypes.arrayOf(PropTypes.string).isRequired,
  onClick: PropTypes.func.isRequired,
  disabled: PropTypes.bool
}

FabEmojis.defaultProps = {
  disabled: false
}

export default FabEmojis
