import { makeStyles } from '@material-ui/core/styles'
import { Colors } from '@conheca-meta-clients/styles'

const useStyles = makeStyles(theme => ({
  container: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.secondary,
    padding: '10px 20px',
    borderRadius: 5,
    paddingRight: 20,
    paddingLeft: 20
  },
  emojisContainer: {
    display: 'grid',
    gridTemplateColumns: 'max-content max-content',
    gridGap: 10,
    justifyContent: 'center',
    alignItems: 'center'
  },
  text: {
    marginRight: 20
  },
  button: {
    marginTop: 5,
    fontSize: '1.7em'
  }
}))

export default useStyles
