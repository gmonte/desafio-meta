import React from 'react'
import PropTypes from 'prop-types'
import Typography from '@material-ui/core/Typography'
import useStyles from './styles'

const IconInfo = (props) => {
  const { Icon, children } = props

  const classes = useStyles()

  return (
    <div className={ classes.container }>
      <Icon className={ classes.icon } />
      <div className={ classes.contentContainer }>
        <Typography variant="body1">
          {children}
        </Typography>
      </div>
    </div>
  )
};

IconInfo.propTypes = {
  Icon: PropTypes.oneOfType([
    PropTypes.func,
    PropTypes.object
  ]).isRequired,
  children: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.string
  ]).isRequired
}

export default IconInfo
