import { makeStyles } from '@material-ui/core/styles'
import { Colors } from '@conheca-meta-clients/styles'

const useStyles = makeStyles(() => ({
  container: {
    width: '100%',
    display: 'inline-flex',
    justifyContent: 'flex-start',
    alignItems: 'center',
    margin: '3px 0'
  },
  icon: {
    color: Colors.muted
  },
  contentContainer: {
    paddingLeft: 10
  }
}))

export default useStyles
