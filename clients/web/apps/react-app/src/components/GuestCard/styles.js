import hexToRgb from 'hex-to-rgba'
import { makeStyles } from '@material-ui/core/styles'
import { Colors } from '@conheca-meta-clients/styles'

const useStyles = makeStyles(() => ({
  container: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'flex-end',
    marginTop: 30,
    marginBottom: 100,
    borderBottomLeftRadius: 0,
    borderBottomRightRadius: 0
  },
  loader: {
    borderBottomLeftRadius: 7,
    borderBottomRightRadius: 7
  },
  cardContainer: {
    width: 450,
    padding: 30,
    paddingTop: 50,
    paddingBottom: 15,
    backgroundColor: hexToRgb(Colors.white, 0.9),
    [`@media (max-width: ${ 454 }px)`]: {
      width: '100% !important'
    }
  },
  cardContainerLoading: {
    borderBottomLeftRadius: 0,
    borderBottomRightRadius: 0
  },
  brandContainer: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 34
  },
  brand: {
    width: '100%'
  }
}))

export default useStyles
