export const getInviteSteps = (invite) => {
  let steps = ['Convite Gerado', 'Convite Visualizado']

  if (invite.status === 'REJECTED') {
    steps = [...steps, 'Convite Recusado']
  } else {
    steps = [...steps, 'Convite Aceito']

    if (invite.type === 'vemPraMeta') {
      steps = [...steps, 'Entrevistado', 'Contratado', 'Prêmio']
    }

    if (invite.type === 'conhecaMeta') {
      steps = [...steps, 'Visitou']
    }
  }

  return steps
}

export const getInviteStatusAsNumber = (invite) => {
  switch (invite.status) {
    case 'VIEWED':
      return 1

    case 'REJECTED':
    case 'ACCEPTED':
      return 2

    case 'VISITED':
      return 3

    case 'HIRED':
      return 4

    case 'PAID_VOUCHER':
      return 5

    default:
      return 0
  }
}
