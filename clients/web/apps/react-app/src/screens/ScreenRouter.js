import React, { Suspense, useEffect } from 'react'
import flow from 'lodash/fp/flow'
import isEmpty from 'lodash/isEmpty'
import withSnackbars from '@conheca-meta-clients/react-snackbars/src/actions/withSnackbars'
import { withStyles } from '@material-ui/core/styles'
import { BrowserRouter as Router } from 'react-router-dom'
import { useSelector, useDispatch } from 'react-redux'
import { Types as AuthTypes } from '@conheca-meta-clients/store/src/ducks/auth'
import { Types as AppTypes } from '@conheca-meta-clients/store/src/ducks/app'
import { selectAuthenticated } from '@conheca-meta-clients/store/src/selectors/auth'
import { firebaseAuth } from '@conheca-meta-clients/s-firebase'
import supportsHistory from '@meta-awesome/functions/src/supportsHistory'
import { CircularFallback } from '@conheca-meta-clients/react-loaders'
import AuthenticatedScreenRouter from './authenticated/AuthenticatedScreenRouter'
import GuestScreenRouter from './guest/GuestScreenRouter'
import globalStyles from '../styles/globalStyles'

const forceRefresh = !supportsHistory()

const ScreenRouter = () => {
  const dispatch = useDispatch()

  const firebaseRecovered = useSelector(state => state.app.firebaseRecovered)
  const authenticated = useSelector(selectAuthenticated)
  const RouterContext = authenticated ? AuthenticatedScreenRouter : GuestScreenRouter

  useEffect(() => {
    firebaseAuth().onAuthStateChanged((user) => {
      if (isEmpty(user) && authenticated) {
        dispatch({ Type: AuthTypes.LOGOUT })
      }
    })
  }, [authenticated, dispatch])

  useEffect(() => {
    firebaseAuth().onAuthStateChanged(() => dispatch({ type: AppTypes.SET_FIREBASE_RECOVERED }))
  }, [dispatch])

  if (!firebaseRecovered) {
    return <CircularFallback />
  }

  return (
    <Router forceRefresh={ forceRefresh }>
      <Suspense fallback={ <CircularFallback /> }>
        <RouterContext />
      </Suspense>
    </Router>
  )
}

export default flow(
  withStyles(globalStyles),
  withSnackbars()
)(ScreenRouter)
