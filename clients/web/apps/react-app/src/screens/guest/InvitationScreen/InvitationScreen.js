import React from 'react'
import InvitationContainer from '../../../containers/InvitationContainer/InvitationContainer'

const InvitationScreen = props => <InvitationContainer { ...props } />

export default InvitationScreen
