import { lazy } from 'react'

const LoginScreen = lazy(() => import('../screens/guest/LoginScreen'))
const InvitationScreen = lazy(() => import('../screens/guest/InvitationScreen'))

const routes = {
  login: {
    path: '/',
    exact: true,
    Component: LoginScreen
  },
  invitation: {
    path: '/invitation/:hash',
    exact: true,
    Component: InvitationScreen
  }
}

export default routes
