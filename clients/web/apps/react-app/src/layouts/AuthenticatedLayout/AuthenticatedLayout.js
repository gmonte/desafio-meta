import React, { useState, useContext } from 'react'
import PropTypes from 'prop-types'
import AppBar from '@material-ui/core/AppBar'
import Divider from '@material-ui/core/Divider'
import Drawer from '@material-ui/core/Drawer'
import Hidden from '@material-ui/core/Hidden'
import IconButton from '@material-ui/core/IconButton'
import MenuIcon from '@material-ui/icons/Menu'
import Toolbar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'
import Fab from '@material-ui/core/Fab'
import { useTheme } from '@material-ui/core/styles'
import UserIconButton from '@conheca-meta-clients/react-icon-buttons/src/components/UserIconButton'
import { DialogContext } from '@conheca-meta-clients/react-dialog'
// import { I18nContext } from '@meta-react/i18n'
// import { Link as RouterLink } from 'react-router-dom'
import PopupPopover from '../../components/PopupPopover'
import InviteFormModal from '../../modals/InviteFormModal/InviteFormModal'
import UserMenu from '../../containers/UserMenu'
import useStyles from './styles'
// import routes from '../../routes/authenticated'
import brand from '../../assets/images/brand.png'

function AuthenticatedLayout(props) {
  const { container, children } = props

  const classes = useStyles()
  const theme = useTheme()

  const [mobileOpen, setMobileOpen] = useState(false)

  const { createDialog } = useContext(DialogContext)

  const openInviteForm = type => createDialog({ id: 'invitationFormModal', Modal: InviteFormModal, props: { type } })


  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen)
  }

  const drawer = (
    <div>
      <div className={ classes.toolbar }>
        <div className={ classes.brandToolbarContainer }>
          <img alt="Meta" src={ brand } className={ classes.brandToolbar } />
        </div>
      </div>
      <Divider />

      <div className={ classes.invites }>
        <Typography variant="caption" className={ classes.inviteText }>
          Envie um novo convite!
        </Typography>

        <Fab
          variant="extended"
          color="secondary"
          onClick={ () => openInviteForm('vemPraMeta') }
          className={ classes.button }
        >
          #VemPraMeta
        </Fab>
        <Fab
          variant="extended"
          color="primary"
          onClick={ () => openInviteForm('conhecaMeta') }
          className={ classes.button }
        >
          #ConheçaAMeta
        </Fab>

      </div>

    </div>
  )

  return (
    <div className={ classes.root }>
      <AppBar color="default" position="fixed" className={ classes.appBar }>
        <Toolbar classes={ { root: classes.toolbarRoot } }>
          <div className={ classes.toolbarRightContainer }>
            <PopupPopover
              parentComponent={ UserIconButton }
              parentProps={ {
                color: 'primary',
                edge: 'end',
                debounce: false
              } }
              component={ UserMenu }
            />
          </div>

          <IconButton
            color="inherit"
            aria-label="open drawer"
            edge="start"
            onClick={ handleDrawerToggle }
            className={ classes.menuButton }
          >
            <MenuIcon />
          </IconButton>
        </Toolbar>
      </AppBar>
      <nav className={ classes.drawer } aria-label="mailbox folders">
        {/* The implementation can be swapped with js to avoid SEO duplication of links. */}
        <Hidden smUp implementation="css">
          <Drawer
            container={ container }
            variant="temporary"
            anchor={ theme.direction === 'rtl' ? 'right' : 'left' }
            open={ mobileOpen }
            onClose={ handleDrawerToggle }
            classes={ {
              paper: classes.drawerPaper
            } }
            ModalProps={ {
              keepMounted: true // Better open performance on mobile.
            } }
          >
            {drawer}
          </Drawer>
        </Hidden>
        <Hidden xsDown implementation="css">
          <Drawer
            classes={ {
              paper: classes.drawerPaper,
            } }
            variant="permanent"
            open
          >
            {drawer}
          </Drawer>
        </Hidden>
      </nav>
      <main className={ classes.content }>
        <div className={ classes.toolbar } />
        {children}
      </main>
    </div>
  )
}

AuthenticatedLayout.propTypes = {
  /**
   * Injected by the documentation to work in an iframe.
   * You won't need it on your project.
   */
  container: PropTypes.element,
  children: PropTypes.element
}

AuthenticatedLayout.defaultProps = {
  children: null,
  container: null
}

export default AuthenticatedLayout
