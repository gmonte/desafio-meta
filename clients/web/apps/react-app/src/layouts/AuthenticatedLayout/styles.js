import { makeStyles } from '@material-ui/core/styles'
import { Fonts, Colors } from '@conheca-meta-clients/styles'

const drawerWidth = 240

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    flex: 1,
    overflowX: 'hidden'
  },
  drawer: {
    [theme.breakpoints.up('sm')]: {
      width: drawerWidth,
      flexShrink: 0,
    },
  },
  appBar: {
    marginLeft: drawerWidth,
    [theme.breakpoints.up('sm')]: {
      width: `calc(100% - ${ drawerWidth }px)`,
    },
  },
  menuButton: {
    marginRight: theme.spacing(2),
    [theme.breakpoints.up('sm')]: {
      display: 'none',
    },
  },
  toolbar: {
    ...theme.mixins.toolbar,
    display: 'flex'
  },
  drawerPaper: {
    width: drawerWidth,
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
    overflowX: 'hidden'
  },
  invites: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 20
  },
  button: {
    fontSize: Fonts.fontSize.L,
    paddingTop: 2,
    paddingBottom: 2,
    marginTop: 15
  },
  brandToolbarContainer: {
    display: 'flex',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  brandToolbar: {
    width: '70%',
    filter: 'grayscale(1) brightness(100)'
  },
  inviteText: {
    color: Colors.grey[4]
  },
  toolbarRightContainer: {
    display: 'flex',
    justifyContent: 'flex-end',
    alignItems: 'center',
    '& > * ': {
      marginLeft: 10
    }
  },
  toolbarRoot: {
    transition: 'all .2s',
    display: 'inline-flex',
    flexDirection: 'row-reverse',
    justifyContent: 'space-between'
  },
}))

export default useStyles
