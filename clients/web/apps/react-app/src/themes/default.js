import hexToRgba from 'hex-to-rgba'
import { Colors, Fonts } from '@conheca-meta-clients/styles'
import Background from '../assets/images/background.jpg'

const gradientColors = [
  `${ hexToRgba(Colors.blue[1], 0.98) } 0%`,
  `${ hexToRgba(Colors.blue[1], 0.95) } 15%`,
  `${ hexToRgba(Colors.blue[1], 0.98) } 40%`,
  `${ hexToRgba(Colors.blue[1], 0.93) } 100%`
  // `${ hexToRgba(Colors.white, 0.9) } 80%`
]

export default {
  palette: {
    primary: {
      main: Colors.primary
    },
    secondary: {
      main: Colors.secondary,
      contrastText: Colors.primary
    }
  },
  typography: {
    useNextVariants: true,
    // Use the system font instead of the default Roboto font.
    fontFamily: Fonts.fontFamily,
    fontWeight: Fonts.fontWeight.regular,
    color: Colors.text
  },
  overrides: {
    MuiButton: {
      label: {
        textTransform: 'none'
      }
    },
    MuiTooltip: {
      popper: {
        zIndex: 5000
      },
      tooltip: {
        margin: '3px !important',
        backgroundColor: Colors.primary,
        color: Colors.white,
        border: `1px solid ${ Colors.blue[1] }`,
        fontSize: 12,
        letterSpacing: 1
      }
    },
    MuiCard: {
      root: {
        boxShadow: `0 1px 4px 0 ${ hexToRgba(Colors.black, 0.14) }`
      }
    },
    MuiFab: {
      extended: {
        padding: '0 25px'
      },
      primary: {
        color: Colors.secondary
      },
      label: {
        textTransform: 'none',
        fontFamily: [Fonts.fontFamilyLobster, Fonts.fontFamily].join(','),
        '& > *': {
          fontFamily: [Fonts.fontFamilyLobster, Fonts.fontFamily].join(',')
        }
      }
    },
    MuiLinearProgress: {
      colorSecondary: {
        backgroundColor: Colors.blue[2]
      },
      barColorSecondary: {
        backgroundColor: Colors.blue[1]
      }
    },
    MuiLink: {
      root: {
        cursor: 'pointer'
      }
    },
    MuiDrawer: {
      paper: {
        '& > *': {
          zIndex: 20,
          position: 'absolute',
          width: '100%',
          height: '100%',
          content: '""',
          display: 'block',
          background: `linear-gradient(${ gradientColors.join(', ') })`
        },
        '&:before': {
          content: '""',
          backgroundColor: 'transparent',
          width: '100%',
          height: '100%',
          position: 'absolute',
          top: 0,
          left: 0,
          zIndex: 10,
          backgroundSize: 'cover',
          backgroundPosition: 'center center',
          backgroundRepeat: 'no-repeat',
          backgroundImage: `url('${ Background }')`
        }
      }
    },
    MuiList: {
      root: {
        '& > a': {
          color: Colors.black
        }
      }
    },
    MuiListItemIcon: {
      root: {
        color: Colors.primary,
        '& > a': {
          color: Colors.primary
        }
      }
    },
    MuiListItemText: {
      root: {
        color: Colors.primary,
        '& > a': {
          color: Colors.primary
        }
      }
    },
    MuiStepper: {
      root: {
        padding: 0,
        background: 'transparent'
      }
    }
  }
}
