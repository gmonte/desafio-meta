import React from 'react'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'
import { useSelector } from 'react-redux'
import VemPraMetaDatagridContainer from '../datagrids/VemPraMetaDatagridContainer'
import ConhecaMetaDatagridContainer from '../datagrids/ConhecaMetaDatagridContainer'
import TabsPanel from '../../components/TabsPanel'
import useStyles from './styles'

const DashboardContainer = () => {
  const classes = useStyles()

  const lastInviteSavedType = useSelector(state => state.invite.invite.data.type)

  return (
    <Grid className={ classes.container } container spacing={ 2 } justify="center">
      <Grid item xs={ 12 }>
        <Typography
          className={ classes.title }
          variant="h5"
          component="h1"
          align="center"
          color="primary"
        >
          Meus convites:
        </Typography>
      </Grid>

      <Grid item xs={ 12 } sm={ 11 }>
        <TabsPanel
          defaultTabType={ lastInviteSavedType }
          tabs={ [
            {
              title: '#VemPraMeta',
              TabContent: VemPraMetaDatagridContainer
            },
            {
              title: '#ConheçaAMeta',
              TabContent: ConhecaMetaDatagridContainer
            }
          ] }
        />
      </Grid>
    </Grid>
  )
}

export default DashboardContainer
