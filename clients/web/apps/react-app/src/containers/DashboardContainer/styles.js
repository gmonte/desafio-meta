import { makeStyles } from '@material-ui/core/styles'
import { Fonts } from '@conheca-meta-clients/styles'

const useStyles = makeStyles(() => ({
  container: {
    marginBottom: 15
  },
  title: {
    fontFamily: [Fonts.fontFamilyLobster, Fonts.fontFamily].join(',')
  }
}))

export default useStyles
