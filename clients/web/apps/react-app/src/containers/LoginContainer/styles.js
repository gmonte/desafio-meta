import { makeStyles } from '@material-ui/core/styles'

const useStyles = makeStyles(theme => ({
  container: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1
  },
  contentContainer: {
    paddingTop: 30
  },
  button: {
    marginTop: 5,
    fontSize: '1.7em'
  },
  extendedIcon: {
    marginRight: theme.spacing(1)
  }
}))

export default useStyles
