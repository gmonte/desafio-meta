import React, { useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { Types as AuthTypes } from '@conheca-meta-clients/store/src/ducks/auth'
import MicrosoftIcon from '@conheca-meta-clients/react-icons/src/MicrosoftIcon'
import Fab from '@material-ui/core/Fab'
import I18n from '@meta-react/i18n'
import GuestCard from '../../components/GuestCard/GuestCard'

import useStyles from './styles'

const LoginScreen = () => {
  const dispatch = useDispatch()
  const loading = useSelector(state => state.auth.loading)
  const error = useSelector(state => state.auth.error)

  const classes = useStyles()

  useEffect(() => {
    if (loading || error) {
      dispatch({ type: AuthTypes.LOGOUT })
    }
    // eslint-disable-next-line
  }, [])

  return (
    <GuestCard
      loading={ loading }
      cardContentProps={ {
        classes: {
          root: classes.contentContainer
        }
      } }
    >
      <div className={ classes.container }>
        <Fab
          variant="extended"
          color="secondary"
          aria-label="login"
          className={ classes.button }
          disabled={ loading }
          onClick={ () => dispatch({ type: AuthTypes.LOGIN }) }
        >
          <MicrosoftIcon className={ classes.extendedIcon } />
          <I18n>i am meta</I18n>
        </Fab>
      </div>
    </GuestCard>
  )
}

export default LoginScreen
