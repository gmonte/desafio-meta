import React, { useContext } from 'react'
import PropTypes from 'prop-types'
import Form from '@conheca-meta-clients/react-inputs/src/Form'
import TextAreaInput from '@conheca-meta-clients/react-inputs/src/TextAreaInput'
import { useForm, useField } from 'react-final-form-hooks'
import I18n, { I18nContext } from '@meta-react/i18n'
import Button from '@material-ui/core/Button'

import useStyles from './styles'

const InvitationReasonFormContainer = (props) => {
  const {
    loading,
    handleClose,
    onSubmit
  } = props

  const { formatMessage } = useContext(I18nContext)
  const classes = useStyles()

  const { form, handleSubmit } = useForm({
    onSubmit
  })

  const fields = {
    reason: useField('reason', form)
  }

  const getErrorMessage = error => (error ? formatMessage({ id: error }) : null)

  return (
    <Form form={ form } onSubmit={ handleSubmit }>
      <TextAreaInput
        { ...fields.reason.input }
        disabled={ loading }
        helperText={ (fields.reason.meta.touched ? getErrorMessage(fields.reason.meta.error) : '') }
        error={ (fields.reason.meta.touched && !!fields.reason.meta.error) }
        fullWidth
      />

      <div className={ classes.buttonsContainer }>
        <Button
          type="button"
          className={ classes.button }
          onClick={ handleClose }
          disabled={ loading }
        >
          <I18n>cancel</I18n>
        </Button>

        <Button
          type="submit"
          variant="contained"
          className={ classes.button }
          disabled={ loading }
        >
          <I18n>reject invite</I18n>
        </Button>
      </div>

    </Form>
  )
}

InvitationReasonFormContainer.propTypes = {
  handleClose: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired,
  loading: PropTypes.bool.isRequired
}

export default InvitationReasonFormContainer
