import React, { useContext } from 'react'
import PropTypes from 'prop-types'
import Form from '@conheca-meta-clients/react-inputs/src/Form'
import EmailInput from '@conheca-meta-clients/react-inputs/src/EmailInput'
import TextInput from '@conheca-meta-clients/react-inputs/src/TextInput'
import PhoneInput from '@conheca-meta-clients/react-inputs/src/PhoneInput'
import CpfCnpjInput from '@conheca-meta-clients/react-inputs/src/CpfCnpjInput'
import SingleFileInput from '@conheca-meta-clients/react-inputs/src/SingleFileInput'
import { useForm, useField } from 'react-final-form-hooks'
import validate from '@meta-awesome/validators'
import validateRequired from '@meta-awesome/validators/src/required'
import validateEmail from '@meta-awesome/validators/src/email'
import validatePhone from '@meta-awesome/validators/src/phone'
import validateCpfCnpj from '@meta-awesome/validators/src/cpfCnpj'
import I18n, { I18nContext } from '@meta-react/i18n'
import Button from '@material-ui/core/Button'
import get from 'lodash/get'
import useStyles from './styles'

const validEmail = (value) => {
  const { errorText } = validate(value, validateRequired, validateEmail)
  return errorText || undefined
}

const validRequired = (value) => {
  const { errorText } = validate(value, validateRequired)
  return errorText || undefined
}

const validPhone = (value) => {
  const { errorText } = validate(value, validateRequired, validatePhone)
  return errorText || undefined
}
const validCpf = (value) => {
  const { errorText } = validate(value, validateRequired, validateCpfCnpj)
  return errorText || undefined
}

const InvitationFormContainer = (props) => {
  const {
    loading, handleClose, invite, onSubmit
  } = props

  const { formatMessage } = useContext(I18nContext)
  const classes = useStyles()

  const { guestName, email, person } = invite

  const { form, handleSubmit } = useForm({
    onSubmit,
    initialValues: {
      name: get(person, 'name') || guestName || '',
      email: get(person, 'email') || email || '',
      cpf: get(person, 'cpf', ''),
      phone: get(person, 'phone', ''),
      urlCV: get(person, 'urlCV') && 'Atualizar currículo'
    }
  })

  const fields = {
    name: useField('name', form, validRequired),
    cpf: useField('cpf', form, validCpf),
    email: useField('email', form, validEmail),
    phone: useField('phone', form, validPhone),
    urlCV: useField('urlCV', form, validRequired)
  }

  const getErrorMessage = error => (error ? formatMessage({ id: error }) : null)

  return (
    <Form form={ form } onSubmit={ handleSubmit }>
      <TextInput
        label={ formatMessage({ id: 'name' }) }
        { ...fields.name.input }
        required
        disabled={ loading }
        helperText={ fields.name.meta.touched ? getErrorMessage(fields.name.meta.error) : '' }
        error={ fields.name.meta.touched && !!fields.name.meta.error }
        fullWidth
      />
      <EmailInput
        label={ formatMessage({ id: 'email' }) }
        { ...fields.email.input }
        required
        disabled={ loading }
        helperText={ fields.email.meta.touched ? getErrorMessage(fields.email.meta.error) : '' }
        error={ fields.email.meta.touched && !!fields.email.meta.error }
        fullWidth
      />
      <CpfCnpjInput
        label="CPF"
        { ...fields.cpf.input }
        required
        disabled={ loading }
        helperText={ fields.cpf.meta.touched ? getErrorMessage(fields.cpf.meta.error) : '' }
        error={ fields.cpf.meta.touched && !!fields.cpf.meta.error }
        fullWidth
        onlyCpf
      />
      <PhoneInput
        label={ formatMessage({ id: 'phone' }) }
        { ...fields.phone.input }
        required
        disabled={ loading }
        helperText={ fields.phone.meta.touched ? getErrorMessage(fields.phone.meta.error) : '' }
        error={ fields.phone.meta.touched && !!fields.phone.meta.error }
        fullWidth
      />
      <SingleFileInput
        chooseFileInputProps={ { pdf: true } }
        label={ formatMessage({ id: 'cv' }) }
        { ...fields.urlCV.input }
        helperText={ fields.urlCV.meta.touched ? getErrorMessage(fields.urlCV.meta.error) : '' }
        error={ fields.urlCV.meta.touched && !!fields.urlCV.meta.error }
        disabled={ loading }
        required
      />

      <div className={ classes.buttonsContainer }>
        <Button type="button" aria-label="login" disabled={ loading } className={ classes.button } onClick={ handleClose }>
          <I18n>cancel</I18n>
        </Button>

        <Button
          type="submit"
          variant="contained"
          color="secondary"
          aria-label="login"
          disabled={ loading }
          className={ classes.button }
        >
          <I18n>confirm</I18n>
        </Button>
      </div>
    </Form>
  )
}

InvitationFormContainer.propTypes = {
  handleClose: PropTypes.func.isRequired,
  loading: PropTypes.bool.isRequired,
  invite: PropTypes.object.isRequired,
  onSubmit: PropTypes.func.isRequired
}

export default InvitationFormContainer
