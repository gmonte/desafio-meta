import { makeStyles } from '@material-ui/core/styles'

const useStyles = makeStyles(() => ({
  buttonsContainer: {
    display: 'flex',
    justifyContent: 'flex-end',
    margin: '10px 0'
  },
  button: {
    margin: '0 3px'
  }
}))

export default useStyles
