import React, { useContext, useState, useEffect } from 'react'
import PropTypes from 'prop-types'
import map from 'lodash/map'
import { useDispatch, useSelector } from 'react-redux'
import { Types as CompanyBranchTypes } from '@conheca-meta-clients/store/src/ducks/companyBranch'
import { Types as InviteTypes } from '@conheca-meta-clients/store/src/ducks/invite'
import Form from '@conheca-meta-clients/react-inputs/src/Form'
import EmailInput from '@conheca-meta-clients/react-inputs/src/EmailInput'
import TextInput from '@conheca-meta-clients/react-inputs/src/TextInput'
import TextAreaInput from '@conheca-meta-clients/react-inputs/src/TextAreaInput'
import AutocompleteInput from '@conheca-meta-clients/react-inputs/src/AutocompleteInput'
import DatePickerInput from '@conheca-meta-clients/react-inputs/src/DatePickerInput'
import { useForm, useField } from 'react-final-form-hooks'
import validate from '@meta-awesome/validators'
import validateRequired from '@meta-awesome/validators/src/required'
import validateEmail from '@meta-awesome/validators/src/email'
// import validateDateTime from '@meta-awesome/validators/src/dateTime'
import I18n, { I18nContext } from '@meta-react/i18n'
import Button from '@material-ui/core/Button'
import useStyles from './styles'

const validEmail = (value) => {
  const { errorText } = validate(value, validateEmail)
  return errorText || undefined
}

const validRequired = (value) => {
  const { errorText } = validate(value, validateRequired)
  return errorText || undefined
}

const validDateTime = (value) => {
  // const { errorText } = validate(value, validateRequired, validateDateTime)
  const { errorText } = validate(value, validateRequired)
  return errorText || undefined
}

const InviteFormContainer = (props) => {
  const { handleClose, type } = props

  const dispatch = useDispatch()
  const loading = useSelector(state => state.invite.invite.loading)
  const companyBranches = useSelector(state => state.companyBranch.data)
  const companyBranchesLoading = useSelector(state => state.companyBranch.loading)

  const [companyBranchesOptions, setCompanyBranchesOptions] = useState([])

  useEffect(() => {
    dispatch({ type: CompanyBranchTypes.GET_COMPANY_BRANCHES })
  }, [dispatch])

  useEffect(() => {
    const newCompanyBranchesOptions = map(companyBranches, ({ id, name }) => ({
      label: name,
      value: id
    }))
    setCompanyBranchesOptions(newCompanyBranchesOptions)
  }, [companyBranches])

  const { formatMessage } = useContext(I18nContext)
  const classes = useStyles()

  const [placeholder] = useState(
    type === 'vemPraMeta' ? 'vem pra meta title placeholder' : 'conheca a meta title placeholder'
  )

  const onSubmit = (values) => {
    dispatch({
      type: InviteTypes.CREATE_INVITE,
      data: {
        ...values,
        type
      },
      callback: ({ dynamicLink }) => {
        window.snackbar.success(dynamicLink, { copyButton: true })
        handleClose()
        if (type === 'vemPraMeta') {
          dispatch({ type: InviteTypes.GET_VEM_PRA_META_INVITES })
        } else if (type === 'conhecaMeta') {
          dispatch({ type: InviteTypes.GET_CONHECA_META_INVITES })
        }
      }
    })
  }

  const { form, handleSubmit } = useForm({
    onSubmit
  })

  const fields = {
    title: useField('title', form, validRequired),
    guestName: useField('guestName', form, validRequired),
    email: useField('email', form, validEmail),
    companyBranchId: useField('companyBranchId', form, validRequired),
    visitDate: useField('visitDate', form, validDateTime)
  }

  const getErrorMessage = error => (error ? formatMessage({ id: error }) : '')

  return (
    <Form form={ form } onSubmit={ handleSubmit }>
      <TextAreaInput
        label={ formatMessage({ id: 'invite title' }) }
        { ...fields.title.input }
        rows={ 2 }
        required
        disabled={ loading }
        placeholder={ formatMessage({ id: placeholder }) }
        helperText={ `${ formatMessage({ id: 'invite title helper text' }) } ${
          fields.title.meta.touched ? getErrorMessage(fields.title.meta.error) : ''
        }` }
        error={ fields.title.meta.touched && !!fields.title.meta.error }
        fullWidth
        autoFocus
      />
      <TextInput
        label={ formatMessage({ id: 'invited name' }) }
        { ...fields.guestName.input }
        required
        disabled={ loading }
        helperText={ fields.guestName.meta.touched ? getErrorMessage(fields.guestName.meta.error) : '' }
        error={ fields.guestName.meta.touched && !!fields.guestName.meta.error }
        fullWidth
      />
      <EmailInput
        label={ formatMessage({ id: 'invited email' }) }
        { ...fields.email.input }
        disabled={ loading }
        helperText={ fields.email.meta.touched ? getErrorMessage(fields.email.meta.error) : '' }
        error={ fields.email.meta.touched && !!fields.email.meta.error }
        fullWidth
      />
      <AutocompleteInput
        label={ formatMessage({
          id: type === 'conhecaMeta' ? 'choose company branch' : 'choose company branch vemPraMeta'
        }) }
        { ...fields.companyBranchId.input }
        isActive={ fields.companyBranchId.meta.active }
        required
        isLoading={ companyBranchesLoading }
        helperText={ fields.companyBranchId.meta.touched ? getErrorMessage(fields.companyBranchId.meta.error) : '' }
        error={ fields.companyBranchId.meta.touched && !!fields.companyBranchId.meta.error }
        options={ companyBranchesOptions }
      />

      <DatePickerInput
        label={ formatMessage({ id: 'invited datetime' }) }
        { ...fields.visitDate.input }
        required
        disabled={ loading }
        helperText={ fields.visitDate.meta.touched ? getErrorMessage(fields.visitDate.meta.error) : '' }
        error={ fields.visitDate.meta.touched && !!fields.visitDate.meta.error }
        fullWidth
        enableTime
      />

      <div className={ classes.buttonsContainer }>
        <Button type="button" aria-label="login" disabled={ loading } className={ classes.button } onClick={ handleClose }>
          <I18n>cancel</I18n>
        </Button>

        <Button
          type="submit"
          variant="contained"
          disabled={ loading }
          color={ type === 'vemPraMeta' ? 'secondary' : 'primary' }
          aria-label="login"
          className={ [classes.button, classes.mainButton].join(' ') }
        >
          <I18n>invite</I18n>
        </Button>
      </div>
    </Form>
  )
}

InviteFormContainer.propTypes = {
  handleClose: PropTypes.func.isRequired,
  type: PropTypes.string.isRequired
}

export default InviteFormContainer
