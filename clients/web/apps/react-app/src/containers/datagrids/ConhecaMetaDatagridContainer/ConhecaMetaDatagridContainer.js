import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { Types as InviteTypes } from '@conheca-meta-clients/store/src/ducks/invite'
import InviteDatagridContainer from '../InviteDatagridContainer'

const ConhecaMetaDatagridContainer = () => {
  const dispatch = useDispatch()

  useEffect(() => {
    dispatch({ type: InviteTypes.GET_CONHECA_META_INVITES })
  }, [dispatch])

  const data = useSelector(state => state.invite.conhecaMeta.data)
  const loading = useSelector(state => state.invite.conhecaMeta.loading)

  return (
    <InviteDatagridContainer
      title="#ConhecaAMeta"
      titleColor="primary"
      loading={ loading }
      data={ data }
      onUpdateClick={ () => dispatch({ type: InviteTypes.GET_CONHECA_META_INVITES }) }
    />
  )
}

export default ConhecaMetaDatagridContainer
