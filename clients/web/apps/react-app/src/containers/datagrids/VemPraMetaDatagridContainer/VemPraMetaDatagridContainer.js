import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { Types as InviteTypes } from '@conheca-meta-clients/store/src/ducks/invite'
import InviteDatagridContainer from '../InviteDatagridContainer'

const VemPraMetaDatagridContainer = () => {
  const dispatch = useDispatch()

  useEffect(() => {
    dispatch({ type: InviteTypes.GET_VEM_PRA_META_INVITES })
  }, [dispatch])

  const data = useSelector(state => state.invite.vemPraMeta.data)
  const loading = useSelector(state => state.invite.vemPraMeta.loading)

  return (
    <InviteDatagridContainer
      title="#VemPraMeta"
      titleColor="secondary"
      loading={ loading }
      data={ data }
      onUpdateClick={ () => dispatch({ type: InviteTypes.GET_VEM_PRA_META_INVITES }) }
    />
  )
}

export default VemPraMetaDatagridContainer
