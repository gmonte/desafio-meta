import React, { useContext } from 'react'
import PropTypes from 'prop-types'
import CopyToClipboard from 'react-copy-to-clipboard'
import ShareIcon from '@conheca-meta-clients/react-icons/src/ShareIcon'
import RefreshIcon from '@conheca-meta-clients/react-icons/src/RefreshIcon'
import Datagrid from '@conheca-meta-clients/react-datagrid'
import Typography from '@material-ui/core/Typography'
import Hidden from '@material-ui/core/Hidden'
import moment from 'moment/moment'
import { momentFriendlyDateTimeFormat } from '@conheca-meta-clients/react-pickers/src/utils'
import { DialogContext } from '@conheca-meta-clients/react-dialog'
import PersonDetailsModal from '../../../modals/PersonDetailsModal'
import StatusStepper from '../../../components/StatusStepper/StatusStepper'
import { getInviteSteps, getInviteStatusAsNumber } from '../../../utils'
import useStyles from './styles'

const InviteDatagridContainer = (props) => {
  const {
    loading, data, title, titleColor, detailPanel, onUpdateClick
  } = props

  const { createDialog } = useContext(DialogContext)

  const openDetailsModal = ({ person, status, reasonRejected }) => createDialog({
    id: 'modal-person-details',
    Modal: PersonDetailsModal,
    props: {
      person,
      status,
      reasonRejected
    }
  })

  const classes = useStyles()

  const columns = [
    {
      hidden: true,
      field: 'id',
      defaultSort: 'desc'
    },
    {
      title: 'Convidado',
      field: 'guestName'
      // defaultGroupOrder: 1
    },
    {
      render: invite => (
        <StatusStepper steps={ getInviteSteps(invite) } activeStep={ getInviteStatusAsNumber(invite) } invite={ invite } />
      )
    },
    {
      hidden: true,
      field: 'companyBranch.name'
    },
    {
      field: 'visitDate',
      title: 'Local/Data/Horário da visita',
      type: 'datetime',
      render: ({ companyBranch = {}, visitDate }) => `${ companyBranch.name }, ${ moment(visitDate)
        .utc(-3)
        .format(momentFriendlyDateTimeFormat)
        .split(' ')
        .join(' às ') }`
    }
  ]

  const actions = [
    {
      icon: () => <RefreshIcon />,
      tooltip: 'Atualizar listagem',
      onClick: onUpdateClick,
      isFreeAction: true,
      iconButtonProps: {
        color: 'primary',
        size: 'small'
      }
    },
    ({ dynamicLink }) => ({
      icon: () => (
        <CopyToClipboard text={ dynamicLink }>
          <ShareIcon color="primary" onClick={ () => window.snackbar.success('Link copiado com sucesso!') } />
        </CopyToClipboard>
      ),
      tooltip: 'Copiar link',
      onClick: () => {}
    })
  ]

  const titleComponent = (
    <Hidden smDown>
      <Typography variant="h4" component="span" color={ titleColor } className={ classes.title }>
        {title}
      </Typography>
    </Hidden>
  )

  return (
    <Datagrid
      title={ titleComponent }
      actions={ actions }
      options={ {
        grouping: false,
        filtering: false,
        columnsButton: false,
        defaultExpanded: true,
        detailPanelCondition: invite => getInviteStatusAsNumber(invite) > 1
      } }
      pageSize={ 4 }
      columns={ columns }
      data={ data }
      loading={ loading }
      detailPanel={ detailPanel }
      onRowClick={ (event, invite, togglePanel) => {
        if (getInviteStatusAsNumber(invite) > 1) {
          openDetailsModal(invite)
          if (detailPanel) {
            togglePanel()
          }
        }
      } }
    />
  )
}

InviteDatagridContainer.propTypes = {
  loading: PropTypes.bool.isRequired,
  data: PropTypes.array.isRequired,
  title: PropTypes.string.isRequired,
  titleColor: PropTypes.string.isRequired,
  onUpdateClick: PropTypes.func.isRequired,
  detailPanel: PropTypes.func
}

InviteDatagridContainer.defaultProps = {
  detailPanel: undefined
}

export default InviteDatagridContainer
