import { makeStyles } from '@material-ui/core/styles'
import { Fonts } from '@conheca-meta-clients/styles'

const useStyles = makeStyles(() => ({
  title: {
    fontFamily: [Fonts.fontFamilyLobster, Fonts.fontFamily].join(',')
  }
}))

export default useStyles
