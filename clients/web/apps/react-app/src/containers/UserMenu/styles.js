export default () => ({
  container: {
    padding: `${ 0 } ${ 20 }px`,
    width: 200
  },
  listItemIcon: {
    minWidth: 40
  }
})
