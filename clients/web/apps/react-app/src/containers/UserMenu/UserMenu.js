import React, { useContext } from 'react'
import PropTypes from 'prop-types'
import flow from 'lodash/fp/flow'
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { Creators as AuthActions } from '@conheca-meta-clients/store/src/ducks/auth'
import { I18nContext } from '@meta-react/i18n'
import { withStyles } from '@material-ui/core/styles'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import ListItemText from '@material-ui/core/ListItemText'
import LogoutIcon from '@conheca-meta-clients/react-icons/src/LogoutIcon'

import styles from './styles'

const UserMenu = (props) => {
  const {
    classes,
    logout,
    onClosePopup
  } = props

  const { formatMessage } = useContext(I18nContext)

  const handleClick = (onClick) => {
    onClick()
    onClosePopup()
  }

  // eslint-disable-next-line react/prop-types
  const createOption = ({ Icon, primary, onClick }) => (
    <ListItem button onClick={ () => handleClick(onClick) }>
      <ListItemIcon classes={ { root: classes.listItemIcon } }>
        <Icon />
      </ListItemIcon>
      <ListItemText primary={ primary } />
    </ListItem>
  )

  return (
    <List>
      {createOption({
        Icon: LogoutIcon,
        primary: formatMessage({ id: 'logout' }),
        onClick: logout
      })}
    </List>
  )
}

UserMenu.propTypes = {
  classes: PropTypes.shape().isRequired,
  history: PropTypes.shape().isRequired,
  onClosePopup: PropTypes.func.isRequired,
  logout: PropTypes.func.isRequired
}

const { logout } = AuthActions
const mapDispatchToProps = dispatch => bindActionCreators({ logout }, dispatch)

export default flow(
  connect(
    null,
    mapDispatchToProps
  ),
  withStyles(styles),
  withRouter
)(UserMenu)
