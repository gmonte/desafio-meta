import React, { useContext, useEffect, useState } from 'react'
import PropTypes from 'prop-types'
import isEmpty from 'lodash/isEmpty'
import { useSelector, useDispatch } from 'react-redux'
import { Types as InviteTypes } from '@conheca-meta-clients/store/src/ducks/invite'
import Typography from '@material-ui/core/Typography'
import MaterialLink from '@material-ui/core/Link'
import Divider from '@material-ui/core/Divider'
import Chip from '@material-ui/core/Chip'
import { Colors } from '@conheca-meta-clients/styles'
import { DialogContext } from '@conheca-meta-clients/react-dialog'
import CalendarIcon from '@conheca-meta-clients/react-icons/src/CalendarIcon'
import MapMarkerIcon from '@conheca-meta-clients/react-icons/src/MapMarkerIcon'
import AccountHeartIcon from '@conheca-meta-clients/react-icons/src/AccountHeartIcon'
import PhoneMessageIcon from '@conheca-meta-clients/react-icons/src/PhoneMessageIcon'
import PhoneIcon from '@conheca-meta-clients/react-icons/src/PhoneIcon'
import { CircularLoader } from '@conheca-meta-clients/react-loaders'
import { momentFriendlyDateTimeFormat } from '@conheca-meta-clients/react-pickers/src/utils'
import moment from 'moment/moment'
import GuestCard from '../../components/GuestCard'
import FabEmojis from '../../components/FabEmojis'
import IconInfo from '../../components/IconInfo'
import InvitationFormModal from '../../modals/InvitationFormModal'
import InvitationReasonFormModal from '../../modals/InvitationReasonFormModal'
import { getInviteStatusAsNumber } from '../../utils'

import useStyles from './styles'

const InvitationScreen = (props) => {
  const {
    match: {
      params: { hash }
    }
  } = props

  const dispatch = useDispatch()

  const [invitationStatusNumber, setInvitationStatusNumber] = useState()

  const invite = useSelector(state => state.invite.invite.data)
  const loading = useSelector(state => state.invite.invite.loading)

  const classes = useStyles()

  const { createDialog, removeDialog } = useContext(DialogContext)

  const submitInvitationForm = values => dispatch({
    type: InviteTypes.ACCEPT_INVITE,
    hash,
    person: values,
    callback: () => {
      removeDialog({ id: 'invitationFormModal' })
    }
  })

  const submitRejectInvitationForm = ({ reason }) => {
    dispatch({
      type: InviteTypes.REJECT_INVITE,
      hash,
      reason,
      callback: () => {
        removeDialog({ id: 'invitationReasonFormModal' })
      }
    })
  }

  const openInvitationForm = () => createDialog({
    id: 'invitationFormModal',
    Modal: InvitationFormModal,
    props: { submitInvitationForm }
  })

  const openInvitationReasonForm = () => createDialog({
    id: 'invitationReasonFormModal',
    Modal: InvitationReasonFormModal,
    props: { submitRejectInvitationForm }
  })

  useEffect(() => {
    setInvitationStatusNumber(getInviteStatusAsNumber(invite))
  }, [invite])

  useEffect(() => {
    dispatch({ type: InviteTypes.GET_INVITE_BY_HASH, hash })
    // eslint-disable-next-line
  }, [])

  const {
    guestName, title, type, visitDate, companyBranch = {}, user = {}, status
  } = invite

  if (isEmpty(invite) || invitationStatusNumber > 2) {
    return (
      <GuestCard
        loading={ loading }
        cardContentProps={ {
          classes: {
            root: classes.contentContainer
          }
        } }
      >
        {invitationStatusNumber > 2 ? (
          <Typography variant="body1" align="center">
            Convite expirado!
          </Typography>
        ) : (
          <CircularLoader />
        )}
      </GuestCard>
    )
  }

  return (
    <GuestCard
      loading={ loading }
      cardContentProps={ {
        classes: {
          root: classes.contentContainer
        }
      } }
    >
      <div className={ classes.container }>
        <Typography variant="h3" className={ classes.congratulations } align="center">
          {guestName}
        </Typography>
        <br />

        <div className={ classes.textContainer }>
          <Typography variant="h5" align="center">
            <b>Você</b>
            <br />
            {type === 'conhecaMeta'
              ? 'está convidado para vir nos conhecer!'
              : 'está convidado para fazer parte do nosso time'}
          </Typography>
          <br />

          <Typography variant="subtitle2" align="center">
            {title}
          </Typography>
          <br />

          <Divider />
          <br />

          <Typography variant="subtitle1" align="justify">
            <b>Sobre a Meta:</b>
          </Typography>

          <Typography variant="body1" align="justify">
            Somos uma empresa com foco em soluções tecnológicas ágeis que simplificam e geram valor real no dia a dia
            das organizações.
          </Typography>
          <br />

          <Typography variant="body1" align="justify">
            Nós queremos te conhecer melhor e te mostrar o que nós fazemos!
          </Typography>
          <br />

          <Divider />
          <br />

          <div className={ classes.statusRowContainer }>
            <IconInfo Icon={ CalendarIcon }>
              <b>
                {visitDate
                  && moment(visitDate)
                    .utc(-3)
                    .format(momentFriendlyDateTimeFormat)
                    .split(' ')
                    .join(' às ')}
              </b>
            </IconInfo>

            {status === 'ACCEPTED' && <Chip color="secondary" size="small" label="Confirmado" />}

            {status === 'REJECTED' && <Chip size="small" label="Recusado" />}
          </div>

          <IconInfo Icon={ MapMarkerIcon }>
            <a href={ companyBranch.linkGoogleMaps } target="_blank" rel="noopener noreferrer">
              {`${ companyBranch.name },`}
              <br />
              {companyBranch.address}
            </a>
          </IconInfo>
          {companyBranch.phone && (
            <IconInfo Icon={ iconProps => <PhoneIcon { ...iconProps } /> }>
              <a href={ `tel:${ companyBranch.phone }` }>{companyBranch.phone}</a>
            </IconInfo>
          )}
          <br />
          <br />

          <IconInfo Icon={ AccountHeartIcon }>
            <span>
              convidado por <b>{user.displayName}</b>
            </span>
          </IconInfo>
          {user.mobilePhone && (
            <IconInfo Icon={ iconProps => <PhoneMessageIcon { ...iconProps } color={ Colors.muted } /> }>
              <a href={ `tel:${ user.mobilePhone }` }>{user.mobilePhone}</a>
            </IconInfo>
          )}
        </div>

        {type === 'vemPraMeta' && (
          <FabEmojis onClick={ openInvitationForm } text="#VemPraMeta" emojis={ ['punch', 'muscle'] } />
        )}
        {type === 'conhecaMeta' && status !== 'ACCEPTED' && (
          <>
            <FabEmojis
              onClick={ () => {
                dispatch({
                  type: InviteTypes.ACCEPT_INVITE,
                  hash,
                  callback: () => {
                    removeDialog({ id: 'invitationFormModal' })
                  }
                })
              } }
              disabled={ status === 'ACCEPTED' }
              text="#ConheçaAMeta"
              emojis={ ['handshake', 'coffee'] }
            />
            <br />
          </>
        )}

        <Typography variant="caption" align="center">
          {status !== 'ACCEPTED' && (
            <>
              <br />
              Clique no botão acima para aceitar o convite.
              <br />
            </>
          )}
          {status !== 'REJECTED' && (
            <>
              {type === 'vemPraMeta' && (
                <>
                  <br />
                  Clique no botão acima para editar as suas informações.
                  <br />
                </>
              )}
              Caso você <b>não</b> possa comparecer,&nbsp;
              <MaterialLink onClick={ openInvitationReasonForm }>clique aqui</MaterialLink>.
            </>
          )}
        </Typography>
      </div>
    </GuestCard>
  )
}

InvitationScreen.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      hash: PropTypes.string
    })
  }).isRequired
}

export default InvitationScreen
