import { makeStyles } from '@material-ui/core/styles'
import { Fonts } from '@conheca-meta-clients/styles'

const useStyles = makeStyles(theme => ({
  container: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1
  },
  contentContainer: {
    paddingTop: 30
  },
  textContainer: {
    marginBottom: 20
  },
  congratulations: {
    fontFamily: [Fonts.fontFamilyLobster, Fonts.fontFamily].join(','),
  },
  statusRowContainer: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center'
  }
}))

export default useStyles
