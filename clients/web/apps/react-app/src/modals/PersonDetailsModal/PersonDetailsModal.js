import React from 'react'
import PropTypes from 'prop-types'
import { Modal } from '@conheca-meta-clients/react-dialog'
import Typography from '@material-ui/core/Typography'
import get from 'lodash/get'
import isEmpty from 'lodash/isEmpty'

const PersonDetailsModal = (props) => {
  const {
    id, open, handleClose, person, status, reasonRejected, ...otherProps
  } = props

  return (
    <Modal id={ id } open={ open } maxWidth="xs" title="Informações do convidado" fullWidth { ...otherProps }>
      <>
        {isEmpty(person) && isEmpty(reasonRejected) && (
          <>
            <Typography variant="body1">Nenhuma informação foi cadastrada pelo convidado</Typography>
            <br />
          </>
        )}

        {!isEmpty(person) && (
          <>
            <Typography variant="h6">Nome: {get(person, 'name', '-')}</Typography>
            <br />

            <Typography variant="h6">Email: {get(person, 'email', '-')}</Typography>
            <br />

            <Typography variant="h6">CPF: {get(person, 'cpf', '-')}</Typography>
            <br />

            <Typography variant="h6">
              Telefone: <a href={ `tel:${ get(person, 'phone', '-') }` }>{get(person, 'phone', '-')}</a>
            </Typography>
            <br />

            <Typography variant="h6">
              Currículo:{' '}
              <a target="_blank" rel="noopener noreferrer" href={ get(person, 'urlCV') }>
                Clique aqui para fazer o download
              </a>
            </Typography>
            <br />
          </>
        )}

        {status === 'REJECTED' && <Typography variant="h6">Motivo da recusa: {reasonRejected || '-'}</Typography>}
      </>
    </Modal>
  )
}

PersonDetailsModal.propTypes = {
  id: PropTypes.string.isRequired,
  open: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
  person: PropTypes.object,
  status: PropTypes.string.isRequired,
  reasonRejected: PropTypes.string
}

PersonDetailsModal.defaultProps = {
  reasonRejected: '',
  person: {}
}

export default PersonDetailsModal
