import React, { useState } from 'react'
import PropTypes from 'prop-types'
import { useSelector } from 'react-redux'
import I18n from '@meta-react/i18n'
import { Modal } from '@conheca-meta-clients/react-dialog'
import { Typography } from '@material-ui/core'
import classNames from 'classnames'
import InviteFormContainer from '../../containers/forms/InviteFormContainer/InviteFormContainer'
import useStyles from './styles'

const InviteFormModal = (props) => {
  const {
    id,
    open,
    handleClose,
    type,
    ...otherProps
  } = props

  const classes = useStyles()

  const [title] = useState(type === 'vemPraMeta' ? '#VemPraMeta' : '#ConheçaAMeta')
  const [subtitle] = useState(type === 'vemPraMeta' ? 'vem pra meta invite info' : 'conheca a meta invite info')

  const loading = useSelector(state => state.invite.invite.loading)

  return (
    <Modal
      id={ id }
      loading={ loading }
      escape={ !loading }
      open={ open }
      maxWidth="xs"
      title={ <span className={ classes.title }>{title}</span> }
      fullWidth
      headerProps={ {
        containerClass: classNames({
          [classes.titleContainerVemPraMeta]: type === 'vemPraMeta',
          [classes.titleContainerConhecaMeta]: type === 'conhecaMeta'
        })
      } }
      { ...otherProps }
    >
      <>
        <Typography variant="body1">
          <I18n>{subtitle}</I18n>
        </Typography>

        <br />

        <InviteFormContainer type={ type } handleClose={ handleClose } />
      </>
    </Modal>
  )
}

InviteFormModal.propTypes = {
  id: PropTypes.string.isRequired,
  open: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
  type: PropTypes.string.isRequired
}

export default InviteFormModal
