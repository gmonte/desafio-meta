import { makeStyles } from '@material-ui/core/styles'
import { Fonts, Colors } from '@conheca-meta-clients/styles'

const useStyles = makeStyles(() => ({
  title: {
    fontFamily: [Fonts.fontFamilyLobster, Fonts.fontFamily].join(','),
    fontSize: Fonts.fontSize.XXXL,
  },
  titleContainerVemPraMeta: {
    backgroundColor: Colors.secondary,
    color: Colors.primary
  },
  titleContainerConhecaMeta: {
    backgroundColor: Colors.primary,
    color: Colors.secondary
  }
}))

export default useStyles
