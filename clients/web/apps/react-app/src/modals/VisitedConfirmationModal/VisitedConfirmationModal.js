import React from 'react'
import PropTypes from 'prop-types'
import { useSelector, useDispatch } from 'react-redux'
import { Types as InviteTypes } from '@conheca-meta-clients/store/src/ducks/invite'
import I18n from '@meta-react/i18n'
import { Modal } from '@conheca-meta-clients/react-dialog'
import Button from '@material-ui/core/Button'
import useStyles from './styles'

const VisitedConfirmationModal = (props) => {
  const {
    id, open, handleClose, invite, ...otherProps
  } = props

  const dispatch = useDispatch()

  const classes = useStyles()

  const { companyBranch = {}, guestName } = invite

  const loading = useSelector(state => state.invite.invite.loading)

  return (
    <Modal
      id={ id }
      loading={ loading }
      escape={ !loading }
      open={ open }
      maxWidth="xs"
      title="Confirmação de visita"
      fullWidth
      contentText={ `Você confirma que "${ guestName }" visitou a sede "${ companyBranch.name }"?` }
      { ...otherProps }
    >
      <div className={ classes.buttonsContainer }>
        <Button type="button" disabled={ loading } className={ classes.button } onClick={ handleClose }>
          <I18n>cancel</I18n>
        </Button>

        <Button
          onClick={ () => dispatch({ type: InviteTypes.SET_VISITED, invite, callback: handleClose }) }
          variant="contained"
          disabled={ loading }
          color="primary"
          className={ [classes.button, classes.mainButton].join(' ') }
        >
          <I18n>confirm</I18n>
        </Button>
      </div>
    </Modal>
  )
}

VisitedConfirmationModal.propTypes = {
  id: PropTypes.string.isRequired,
  open: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
  invite: PropTypes.object.isRequired
}

export default VisitedConfirmationModal
