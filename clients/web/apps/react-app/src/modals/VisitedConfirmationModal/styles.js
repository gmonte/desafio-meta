import { makeStyles } from '@material-ui/core/styles'
import { Fonts, Colors } from '@conheca-meta-clients/styles'

const useStyles = makeStyles(() => ({
  title: {
    fontFamily: [Fonts.fontFamilyLobster, Fonts.fontFamily].join(','),
    fontSize: Fonts.fontSize.XXXL
  },
  titleContainerVemPraMeta: {
    backgroundColor: Colors.secondary,
    color: Colors.primary
  },
  titleContainerConhecaMeta: {
    backgroundColor: Colors.primary,
    color: Colors.secondary
  },
  buttonsContainer: {
    display: 'flex',
    justifyContent: 'flex-end',
    margin: '10px 0'
  },
  button: {
    margin: '0 3px'
  }
}))

export default useStyles
