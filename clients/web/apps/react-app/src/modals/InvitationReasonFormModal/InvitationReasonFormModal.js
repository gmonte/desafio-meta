import React from 'react'
import PropTypes from 'prop-types'
import { Modal } from '@conheca-meta-clients/react-dialog'
import InvitationReasonFormContainer from '../../containers/forms/InvitationReasonFormContainer'
import { Typography } from '@material-ui/core'
import { useSelector } from 'react-redux'

const InvitationReasonFormModal = (props) => {
  const {
    id,
    open,
    handleClose,
    submitRejectInvitationForm,
    ...otherProps
  } = props

  const loading = useSelector(state => state.invite.invite.loading)

  return (
    <Modal
      open={ open }
      id={ id }
      maxWidth="xs"
      title="ooh :("
      fullWidth
      loading={ loading }
      escape={ !loading }
      { ...otherProps }
    >
      <>
        <Typography variant="body1">
          Tudo bem, entendemos que você pode não estar disponível...
        </Typography>

        <br />

        <Typography variant="body1">
          Caso ache oportuno, nos informe o <b>motivo</b> da recusa.
          Trabalharemos para que esse encontro seja possível quando você tiver disponibilidade:
        </Typography>

        <InvitationReasonFormContainer
          loading={ loading }
          handleClose={ handleClose }
          onSubmit={ submitRejectInvitationForm }
        />
      </>
    </Modal>
  )
}

InvitationReasonFormModal.propTypes = {
  id: PropTypes.string.isRequired,
  open: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
  submitRejectInvitationForm: PropTypes.func.isRequired
}

export default InvitationReasonFormModal
