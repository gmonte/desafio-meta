import React, { useContext } from 'react'
import PropTypes from 'prop-types'
import I18n, { I18nContext } from '@meta-react/i18n'
import { Modal } from '@conheca-meta-clients/react-dialog'
import { useSelector } from 'react-redux'
import InvitationFormContainer from '../../containers/forms/InvitationFormContainer'
import { Typography } from '@material-ui/core'

const InvitationFormModal = (props) => {
  const {
    id,
    open,
    handleClose,
    submitInvitationForm,
    ...otherProps
  } = props

  const loading = useSelector(state => state.invite.invite.loading)
  const invite = useSelector(state => state.invite.invite.data)

  const { formatMessage } = useContext(I18nContext)

  return (
    <Modal
      id={ id }
      open={ open }
      loading={ loading }
      escape={ !loading }
      maxWidth="xs"
      title={ formatMessage({ id: 'personal informations' }) }
      fullWidth
      { ...otherProps }
    >
      <>
        <Typography variant="body1">
          <I18n>enter your informations</I18n>
        </Typography>

        <br />

        <InvitationFormContainer
          loading={ loading }
          handleClose={ handleClose }
          invite={ invite }
          onSubmit={ submitInvitationForm }
        />
      </>
    </Modal>
  )
}

InvitationFormModal.propTypes = {
  id: PropTypes.string.isRequired,
  open: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
  submitInvitationForm: PropTypes.func.isRequired
}

export default InvitationFormModal
