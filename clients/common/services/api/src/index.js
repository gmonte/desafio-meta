import get from 'lodash/get'
import http from '@conheca-meta-clients/s-http'
import { getAuthorizationHeader } from '@conheca-meta-clients/s-firebase/src/auth'

async function api(options = {}) {
  try {

    const { auth, ...otherOptions } = options
    let headers = {
      'Content-Type': 'application/json',
      ...get(options, 'headers', {})
    }

    if (auth) {
      const bearerToken = await getAuthorizationHeader()
      headers = {
        ...headers,
        'Authorization': bearerToken
      }
    }

    const response = await http({
      baseURL: process.env.REACT_APP_API_URL,
      mode: 'no-cors',
      ...otherOptions,
      headers
    })

    return response
  } catch (e) {
    const error = get(e, 'response.data.error', e)
    throw error
  }
}

export default api
