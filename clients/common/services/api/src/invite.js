import get from 'lodash/get'
import api from './index'

export const getInvitesVemPraMeta = async (options = {}) => api({
  method: 'GET',
  url: '/invite/invites',
  auth: true,
  ...options,
  data: {
    ...get(options, 'data', {}),
    type: 'vemPraMeta'
  }
})

export const getInvitesConhecaMeta = async (options = {}) => api({
  method: 'GET',
  url: '/invite/invites',
  auth: true,
  ...options,
  data: {
    ...get(options, 'data', {}),
    type: 'conhecaMeta'
  }
})

export const getInviteByHash = async (options = {}) => {
  const {
    data: { hash, ...data }
  } = options

  return api({
    method: 'GET',
    url: `/invite/${ options.data.hash }`,
    ...options,
    data
  })
}

export const postInviteUpdateLink = async (options = {}) => api({
  method: 'POST',
  url: '/invite',
  auth: true,
  ...options
})

export const postInvite = async (options = {}) => api({
  method: 'POST',
  url: '/invite',
  auth: true,
  ...options,
  data: {
    ...options.data,
    status: getInviteNextStatus(options.data)
  }
})

export const putSetInviteViewed = async (options = {}) => api({
  method: 'PUT',
  url: '/invite/statusViewed',
  ...options,
  params: {
    uuid: options.params.hash
  }
})

export const putRejectInvite = async (options = {}) => {
  const {
    data: { hash, ...data }
  } = options

  return api({
    method: 'PUT',
    url: '/invite/statusRejected',
    ...options,
    params: {
      uuid: hash
    },
    data
  })
}

export const postAcceptInvite = async (options = {}) => {
  const {
    data: { hash, ...data }
  } = options

  return api({
    method: 'POST',
    url: '/invite/accept',
    ...options,
    data: {
      ...data,
      uuid: hash
    }
  })
}

export const getInviteNextStatus = (invite) => {
  const { status } = invite

  switch (status) {
    case 'CREATED':
      return 'VIEWED'

    case 'REJECTED':
    case 'VIEWED':
      return 'ACCEPTED'

    case 'ACCEPTED':
      return 'VISITED'

    case 'VISITED':
      return 'HIRED'

    case 'HIRED':
      return 'PAID_VOUCHER'

    default:
      return 'CREATED'
  }
}
