import api from './index'

export const postCreateUser = (options = {}) => api({
  method: 'POST',
  url: '/user',
  auth: true,
  ...options,
})
