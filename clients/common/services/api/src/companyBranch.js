import api from './index'

export const getAllCompanyBranches = (options = {}) => api({
  method: 'GET',
  url: '/companybranch/all',
  auth: true,
  ...options
})
