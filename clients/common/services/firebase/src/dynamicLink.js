import get from 'lodash/get'
import http from '@conheca-meta-clients/s-http'

async function firebaseDynamicLinksApi(options = {}) {
  try {

    const headers = {
      'Content-Type': 'application/json',
      ...get(options, 'headers', {})
    }

    const response = await http({
      baseURL: 'https://firebasedynamiclinks.googleapis.com/v1/',
      ...options,
      headers
    })

    return response
  } catch (e) {
    const error = get(e, 'response.data.error', e)
    throw error
  }
}

export const createDynamicLink = async (link) => {
  const { data: { shortLink } } = await firebaseDynamicLinksApi({
    method: 'POST',
    url: '/shortLinks',
    params: {
      key: process.env.REACT_APP_FIREBASE_API_KEY
    },
    data: {
      longDynamicLink: link
    }
  })

  return shortLink
}
