import flow from 'lodash/fp/flow'
import upperCase from 'lodash/fp/upperCase'
import join from 'lodash/fp/join'
import split from 'lodash/fp/split'
import uploadFile from './uploadFile'

export const uploadCV = async ({ file, userName, userCpf }) => {
  const { extension } = file

  const fileName = flow(
    upperCase,
    split(' '),
    join('_')
  )(userName)

  const fileCpf = flow(
    split('.'),
    join(''),
    split('-'),
    join('')
  )(userCpf)

  const url = await uploadFile({
    file,
    fileName: `${ fileName }_${ fileCpf }.${ extension }`
  })

  return url
}
