import { firebaseAuth } from './index'

export const signInWithEmailAndPassword = async ({ email, password }) => firebaseAuth().signInWithEmailAndPassword(email, password)

export const createUserWithEmailAndPassword = async ({ email, password }) => firebaseAuth().createUserWithEmailAndPassword(email, password)

export const signInWithFacebook = async () => {
  const provider = new firebaseAuth.FacebookAuthProvider()
  return firebaseAuth().signInWithPopup(provider)
}

export const signInWithGoogle = async () => {
  const provider = new firebaseAuth.GoogleAuthProvider()
  return firebaseAuth().signInWithPopup(provider)
}

export const signInWithMicrosoft = async () => {
  const provider = new firebaseAuth.OAuthProvider('microsoft.com')
  provider.setCustomParameters({
    tenant: 'meta.com.br'
  })
  return firebaseAuth().signInWithPopup(provider)
}

export const getAuthorizationHeader = async () => {
  const idToken = await firebaseAuth().currentUser.getIdToken()
  return `Bearer ${ idToken }`
}

export const signOut = async () => firebaseAuth().signOut()
