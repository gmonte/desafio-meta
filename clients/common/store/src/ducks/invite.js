import { createActions, createReducer } from 'reduxsauce'

/*
 * Creating action types & creators
 * */
export const { Types, Creators } = createActions({
  createInvite: ['data', 'callback'],
  createInviteSuccess: ['invite'],
  updateInvite: ['id', 'data', 'callback'],
  updateInviteSuccess: [],

  getInviteByHash: ['hash'],
  getInviteSuccess: ['invite'],
  setInviteError: ['error'],

  getVemPraMetaInvites: [],
  getVemPraMetaInvitesSuccess: ['data'],
  getVemPraMetaInvitesError: ['error'],

  getConhecaMetaInvites: [],
  getConhecaMetaInvitesSuccess: ['data'],
  getConhecaMetaInvitesError: ['error'],

  acceptInvite: ['hash', 'person', 'callback'],
  acceptInviteSuccess: ['invite'],
  acceptInviteError: ['error'],

  rejectInvite: ['hash', 'reason', 'callback'],
  rejectInviteSuccess: ['invite'],
  rejectInviteError: ['error'],

  setVisited: ['invite', 'callback'],
  setVisitedSuccess: [],
  setVisitedError: ['error'],

  logout: []
})

/*
 * Creating Initial state and Redux handlers
 * */
const INITIAL_STATE = {
  invite: {
    loading: false,
    error: null,
    data: {}
  },
  vemPraMeta: {
    loading: false,
    error: null,
    data: []
  },
  conhecaMeta: {
    loading: false,
    error: null,
    data: []
  }
}

const createInvite = (state = INITIAL_STATE) => ({
  ...state,
  invite: {
    ...state.invite,
    loading: true,
    error: INITIAL_STATE.invite.error
  }
})

const createInviteSuccess = (state = INITIAL_STATE, { invite }) => ({
  ...state,
  invite: {
    ...state.invite,
    loading: false,
    data: invite
  }
})

const updateInvite = (state = INITIAL_STATE) => ({
  ...state,
  invite: {
    ...state.invite,
    loading: true,
    error: INITIAL_STATE.invite.error
  }
})

const updateInviteSuccess = (state = INITIAL_STATE) => ({
  ...state,
  invite: {
    ...state.invite,
    loading: false
  }
})

const getInvite = (state = INITIAL_STATE) => ({
  ...state,
  invite: {
    ...state.invite,
    loading: true,
    error: INITIAL_STATE.invite.error
  }
})

const getInviteSuccess = (state = INITIAL_STATE, { invite }) => ({
  ...state,
  invite: {
    ...state.invite,
    loading: false,
    data: invite
  }
})

const setInviteError = (state = INITIAL_STATE, { error }) => ({
  ...state,
  invite: {
    ...state.invite,
    loading: false,
    error
  }
})

const getVemPraMetaInvites = (state = INITIAL_STATE) => ({
  ...state,
  vemPraMeta: {
    ...state.vemPraMeta,
    loading: true,
    error: INITIAL_STATE.conhecaMeta.error
  }
})

const getVemPraMetaInvitesSuccess = (state = INITIAL_STATE, { data }) => ({
  ...state,
  vemPraMeta: {
    ...state.vemPraMeta,
    loading: false,
    data
  }
})

const getVemPraMetaInvitesError = (state = INITIAL_STATE, { error }) => ({
  ...state,
  vemPraMeta: {
    ...state.vemPraMeta,
    loading: false,
    error
  }
})

const getConhecaMetaInvites = (state = INITIAL_STATE) => ({
  ...state,
  conhecaMeta: {
    ...state.conhecaMeta,
    loading: true,
    error: INITIAL_STATE.conhecaMeta.error
  }
})

const getConhecaMetaInvitesSuccess = (state = INITIAL_STATE, { data }) => ({
  ...state,
  conhecaMeta: {
    ...state.conhecaMeta,
    loading: false,
    data
  }
})

const getConhecaMetaInvitesError = (state = INITIAL_STATE, { error }) => ({
  ...state,
  conhecaMeta: {
    ...state.conhecaMeta,
    loading: false,
    error
  }
})

const acceptInvite = (state = INITIAL_STATE) => ({
  ...state,
  invite: {
    ...state.invite,
    loading: true,
    error: INITIAL_STATE.invite.error
  }
})

const acceptInviteSuccess = (state = INITIAL_STATE, { invite }) => ({
  ...state,
  invite: {
    ...state.invite,
    loading: false,
    data: invite
  }
})

const acceptInviteError = (state = INITIAL_STATE, { error }) => ({
  ...state,
  invite: {
    ...state.invite,
    loading: false,
    error
  }
})

const rejectInvite = (state = INITIAL_STATE) => ({
  ...state,
  invite: {
    ...state.invite,
    loading: true,
    error: INITIAL_STATE.invite.error
  }
})

const rejectInviteSuccess = (state = INITIAL_STATE, { invite }) => ({
  ...state,
  invite: {
    ...state.invite,
    loading: false,
    data: invite
  }
})

const rejectInviteError = (state = INITIAL_STATE, { error }) => ({
  ...state,
  invite: {
    ...state.invite,
    loading: false,
    error
  }
})

const setVisited = (state = INITIAL_STATE) => ({
  ...state,
  invite: {
    ...state.invite,
    loading: true,
    error: INITIAL_STATE.invite.error
  }
})

const setVisitedSuccess = (state = INITIAL_STATE) => ({
  ...state,
  invite: {
    ...state.invite,
    loading: false
  }
})

const setVisitedError = (state = INITIAL_STATE, { error }) => ({
  ...state,
  invite: {
    ...state.invite,
    loading: false,
    error
  }
})

const logout = () => ({ ...INITIAL_STATE })

/*
 * Creating reducer
 * */
export default createReducer(INITIAL_STATE, {
  [Types.CREATE_INVITE]: createInvite,
  [Types.CREATE_INVITE_SUCCESS]: createInviteSuccess,
  [Types.UPDATE_INVITE]: updateInvite,
  [Types.UPDATE_INVITE_SUCCESS]: updateInviteSuccess,

  [Types.GET_INVITE_BY_HASH]: getInvite,
  [Types.GET_INVITE_SUCCESS]: getInviteSuccess,
  [Types.SET_INVITE_ERROR]: setInviteError,

  [Types.GET_VEM_PRA_META_INVITES]: getVemPraMetaInvites,
  [Types.GET_VEM_PRA_META_INVITES_SUCCESS]: getVemPraMetaInvitesSuccess,
  [Types.GET_VEM_PRA_META_INVITES_ERROR]: getVemPraMetaInvitesError,
  [Types.GET_CONHECA_META_INVITES]: getConhecaMetaInvites,
  [Types.GET_CONHECA_META_INVITES_SUCCESS]: getConhecaMetaInvitesSuccess,
  [Types.GET_CONHECA_META_INVITES_ERROR]: getConhecaMetaInvitesError,

  [Types.ACCEPT_INVITE]: acceptInvite,
  [Types.ACCEPT_INVITE_SUCCESS]: acceptInviteSuccess,
  [Types.ACCEPT_INVITE_ERROR]: acceptInviteError,

  [Types.REJECT_INVITE]: rejectInvite,
  [Types.REJECT_INVITE_SUCCESS]: rejectInviteSuccess,
  [Types.REJECT_INVITE_ERROR]: rejectInviteError,

  [Types.SET_VISITED]: setVisited,
  [Types.SET_VISITED_SUCCESS]: setVisitedSuccess,
  [Types.SET_VISITED_ERROR]: setVisitedError,

  [Types.LOGOUT]: logout
})
