import { createActions, createReducer } from 'reduxsauce'

/*
 * Creating action types & creators
 * */
export const { Types, Creators } = createActions({
  setFirebaseRecovered: []
})

/*
 * Creating Initial state and Redux handlers
 * */
const INITIAL_STATE = {
  firebaseRecovered: false
}

const setFirebaseRecovered = (state = INITIAL_STATE) => ({
  ...state,
  firebaseRecovered: true
})

/*
 * Creating reducer
 * */
export default createReducer(INITIAL_STATE, {
  [Types.SET_FIREBASE_RECOVERED]: setFirebaseRecovered
})
