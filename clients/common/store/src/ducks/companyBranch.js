import { createActions, createReducer } from 'reduxsauce'

/*
 * Creating action types & creators
 * */
export const { Types, Creators } = createActions({
  getCompanyBranches: [],
  getCompanyBranchesSuccess: ['data'],
  getCompanyBranchesError: ['error'],
  logout: []
})

/*
 * Creating Initial state and Redux handlers
 * */
const INITIAL_STATE = {
  loading: false,
  error: null,
  data: []
}

const getCompanyBranches = (state = INITIAL_STATE, action) => ({
  ...state,
  loading: true,
  error: INITIAL_STATE.error
})

const getCompanyBranchesSuccess = (state = INITIAL_STATE, { data }) => ({
  ...state,
  loading: false,
  data
})

const getCompanyBranchesError = (state = INITIAL_STATE, { error }) => ({
  ...state,
  loading: false,
  error
})

const logout = () => ({ ...INITIAL_STATE })

/*
 * Creating reducer
 * */
export default createReducer(INITIAL_STATE, {
  [Types.GET_COMPANY_BRANCHES]: getCompanyBranches,
  [Types.GET_COMPANY_BRANCHES_SUCCESS]: getCompanyBranchesSuccess,
  [Types.GET_COMPANY_BRANCHES_ERROR]: getCompanyBranchesError,
  [Types.LOGOUT]: logout
})
