import getCompanyBranches from './getCompanyBranches'

export default [
  ...getCompanyBranches
]
