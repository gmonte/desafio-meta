import {
  takeLatest, put, call
} from 'redux-saga/effects'
import { getAllCompanyBranches } from '@conheca-meta-clients/s-api/src/companyBranch'
import { Types } from '../../ducks/companyBranch'

function* getCompanyBranches() {
  try {
    const { data } = yield call(getAllCompanyBranches)

    yield put({
      type: Types.GET_COMPANY_BRANCHES_SUCCESS,
      data
    })
  } catch (e) {
    yield put({
      type: Types.GET_COMPANY_BRANCHES_ERROR,
      error: e.toString()
    })

    window.snackbar.error(e.toString())
  }
}

export default [takeLatest(Types.GET_COMPANY_BRANCHES, getCompanyBranches)]
