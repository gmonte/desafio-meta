import { takeLatest, put, call } from 'redux-saga/effects'
import { postInvite } from '@conheca-meta-clients/s-api/src/invite'

import { Types } from '../../ducks/invite'

function* setVisited({ invite, callback }) {
  try {
    yield call(postInvite, {
      data: {
        id: invite.id,
        status: 'ACCEPTED'
      }
    })

    yield put({ type: Types.SET_VISITED_SUCCESS })

    if (invite.type === 'conhecaMeta') {
      yield put({ type: Types.GET_CONHECA_META_INVITES })
    } else if (invite.type === 'vemPraMeta') {
      yield put({ type: Types.GET_VEM_PRA_META_INVITES })
    }

    yield call(callback)
  } catch (e) {
    yield put({
      type: Types.SET_VISITED_ERROR,
      error: e.toString()
    })
  }
}

export default [takeLatest(Types.SET_VISITED, setVisited)]
