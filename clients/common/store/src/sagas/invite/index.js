import createInvite from './createInvite'
import getVemPraMetaInvites from './getVemPraMetaInvites'
import getConhecaMetaInvites from './getConhecaMetaInvites'
import getInviteByHash from './getInviteByHash'
import acceptInvite from './acceptInvite'
import rejectInvite from './rejectInvite'
import setVisited from './setVisited'

export default [
  ...createInvite,
  ...getVemPraMetaInvites,
  ...getConhecaMetaInvites,
  ...getInviteByHash,
  ...acceptInvite,
  ...rejectInvite,
  ...setVisited
]
