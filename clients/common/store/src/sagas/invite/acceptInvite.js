import {
  takeLatest, put, call, select
} from 'redux-saga/effects'
import get from 'lodash/get'
import { postAcceptInvite } from '@conheca-meta-clients/s-api/src/invite'
import { uploadCV } from '@conheca-meta-clients/s-firebase/src/uploadCV'

import { Types } from '../../ducks/invite'

function* acceptInvite({ hash, person, callback }) {
  try {
    const invite = yield select(state => get(state, 'invite.invite.data', {}))

    if (invite.type === 'vemPraMeta') {
      if (person.urlCV !== 'Atualizar currículo') {
        const urlCV = yield call(uploadCV, { file: person.urlCV, userName: person.name, userCpf: person.cpf })
        person = {
          ...person,
          urlCV
        }
      }
    }

    const { data: newInvite } = yield call(postAcceptInvite, {
      data: {
        hash,
        ...(person ? { person } : {})
      }
    })

    yield put({
      type: Types.ACCEPT_INVITE_SUCCESS,
      invite: {
        ...invite,
        ...newInvite
      }
    })

    yield call(callback)
    window.snackbar.success('Obrigado!!! Estamos ansiosos para recebê-lo. Com certeza será sensacional!!', {
      autoHideDuration: 10000
    })
  } catch (e) {
    window.snackbar.error(e.toString())
    yield put({
      type: Types.ACCEPT_INVITE_ERROR,
      error: e.toString()
    })
  }
}

export default [takeLatest(Types.ACCEPT_INVITE, acceptInvite)]
