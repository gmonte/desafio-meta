import {
  takeLatest, put, call
} from 'redux-saga/effects'
import { getInvitesVemPraMeta } from '@conheca-meta-clients/s-api/src/invite'
import { Types } from '../../ducks/invite'

function* getVemPraMetaInvites() {
  try {
    const { data } = yield call(getInvitesVemPraMeta)

    yield put({ type: Types.GET_VEM_PRA_META_INVITES_SUCCESS, data })
  } catch (e) {
    yield put({
      type: Types.SET_INVITE_ERROR,
      error: e.toString()
    })
  }
}

export default [takeLatest(Types.GET_VEM_PRA_META_INVITES, getVemPraMetaInvites)]
