import {
  takeLatest, put, call
} from 'redux-saga/effects'
import { postInvite } from '@conheca-meta-clients/s-api/src/invite'
import getLocationUrl from '@meta-awesome/functions/src/getLocationUrl'
import { Types } from '../../ducks/invite'

function* createInvite({ data, callback }) {
  try {
    let internalLink = `page.link/?link=${ getLocationUrl() }/invitation`

    if (data.type === 'conhecaMeta') {
      internalLink = `https://conhecaameta.${ internalLink }`
    } else if (data.type === 'vemPraMeta') {
      internalLink = `https://vemprameta.${ internalLink }`
    }

    const { data: invite } = yield call(postInvite, {
      data: {
        ...data,
        internalLink
      }
    })

    yield call(callback, invite)

    yield put({ type: Types.CREATE_INVITE_SUCCESS, invite })
  } catch (e) {
    yield put({
      type: Types.SET_INVITE_ERROR,
      error: e.toString()
    })

    window.snackbar.error(e.toString())
  }
}

export default [takeLatest(Types.CREATE_INVITE, createInvite)]
