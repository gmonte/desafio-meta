import {
  takeLatest, put, call
} from 'redux-saga/effects'
import { getInviteByHash as getInviteByHashService, putSetInviteViewed } from '@conheca-meta-clients/s-api/src/invite'
import { Types } from '../../ducks/invite'

function* getInviteByHash({ hash }) {
  try {
    let { data: invite } = yield call(getInviteByHashService, {
      data: {
        hash
      }
    })

    if (invite.status === 'CREATED') {
      const newInvite = yield call(putSetInviteViewed, {
        params: {
          hash
        }
      })

      invite = {
        ...invite,
        ...newInvite
      }
    }

    yield put({ type: Types.GET_INVITE_SUCCESS, invite })
  } catch (e) {
    yield put({
      type: Types.SET_INVITE_ERROR,
      error: e.toString()
    })
  }
}

export default [takeLatest(Types.GET_INVITE_BY_HASH, getInviteByHash)]
