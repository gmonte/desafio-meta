import {
  takeLatest, put, call
} from 'redux-saga/effects'
import { putRejectInvite } from '@conheca-meta-clients/s-api/src/invite'

import { Types } from '../../ducks/invite'

function* rejectInvite({ hash, reason, callback }) {
  try {

    const { data: invite } = yield call(putRejectInvite, {
      data: {
        hash,
        reason
      }
    })

    yield put({ type: Types.REJECT_INVITE_SUCCESS, invite })

    yield call(callback)
    window.snackbar.info('Convite recusado... Mas se mudar de ideia, você pode confirmá-lo por aqui! Obrigado.', {
      autoHideDuration: 10000
    })
  } catch (e) {
    yield put({
      type: Types.REJECT_INVITE_ERROR,
      error: e.toString()
    })
  }
}

export default [takeLatest(Types.REJECT_INVITE, rejectInvite)]
