import {
  takeLatest, put, call
} from 'redux-saga/effects'
import { getInvitesConhecaMeta } from '@conheca-meta-clients/s-api/src/invite'
import { Types } from '../../ducks/invite'

function* getConhecaMetaInvites() {
  try {
    const { data } = yield call(getInvitesConhecaMeta)

    yield put({ type: Types.GET_CONHECA_META_INVITES_SUCCESS, data })
  } catch (e) {
    yield put({
      type: Types.SET_INVITE_ERROR,
      error: e.toString()
    })
  }
}

export default [takeLatest(Types.GET_CONHECA_META_INVITES, getConhecaMetaInvites)]
