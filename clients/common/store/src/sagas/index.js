import { all } from 'redux-saga/effects'
import auth from './auth'
import companyBranch from './companyBranch'
import invite from './invite'

export default function* root() {
  yield all([...auth, ...companyBranch, ...invite])
}
