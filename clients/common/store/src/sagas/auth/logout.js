import {
  takeLatest, call
} from 'redux-saga/effects'
import { signOut } from '@conheca-meta-clients/s-firebase/src/auth'
import { Types } from '../../ducks/auth'

function* logout() {
  yield call(signOut)
}

export default [takeLatest(Types.LOGOUT, logout)]
