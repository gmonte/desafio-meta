import {
  takeLatest, put, call
} from 'redux-saga/effects'
import { signInWithMicrosoft } from '@conheca-meta-clients/s-firebase/src/auth'
import { postCreateUser } from '@conheca-meta-clients/s-api/src/user'
import { Types } from '../../ducks/auth'

function* login() {
  try {

    const response = yield call(signInWithMicrosoft)

    const {
      user: {
        uid: id
      },
      additionalUserInfo: {
        profile: {
          displayName,
          jobTitle,
          mail,
          mobilePhone
        }
      }
    } = response

    const user = {
      id,
      displayName,
      jobTitle,
      mail,
      mobilePhone
    }

    yield call(postCreateUser, { data: { ...user } })

    yield put({
      type: Types.LOGIN_SUCCESS,
      user
    })
  } catch (e) {
    yield put({
      type: Types.LOGIN_ERROR,
      error: e.toString()
    })

    window.snackbar.error(e.toString())
  }
}

export default [takeLatest(Types.LOGIN, login)]
