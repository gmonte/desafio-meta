const routes = require('express').Router()
const cors = require('cors')

const { createUser, checkIfAuthenticated } = require('../../server/auth')

/** Invite */
const { getInvite, getInviteUuid } = require('../routes/invite/get')
const { getAllInvites, getInvites } = require('../routes/invite/all/get')
const { saveInvite } = require('../routes/invite/post')
const { deleteInvite } = require('../routes/invite/delete')
const { acceptInvite } = require('../routes/invite/accept/post')
const { setStatusViwed, setStatusRejected } = require('../routes/invite/statusChanges/put')


/** CompanyBranch */
const { getAllCompanyBranches } = require('../routes/companyBranch/get')

/** User */
const { getUser } = require('../routes/user/get')
const { saveUser } = require('../routes/user/post')

/** Person */
const { getPerson } = require('../routes/person/get')
const { savePerson } = require('../routes/person/post')

routes.get('/invite', checkIfAuthenticated, getInvite)
routes.get('/invite/all', checkIfAuthenticated, getAllInvites)
routes.get('/invite/invites', checkIfAuthenticated, getInvites)
routes.get('/invite/:uuid', getInviteUuid)
routes.post('/invite', checkIfAuthenticated, saveInvite)
routes.delete('/invite', checkIfAuthenticated, deleteInvite)
routes.post('/invite/accept', acceptInvite)
routes.put('/invite/statusViewed', setStatusViwed)
routes.put('/invite/statusRejected', setStatusRejected)

routes.get('/user', checkIfAuthenticated, getUser)
routes.post('/user', checkIfAuthenticated, saveUser)

routes.get('/person', getPerson)
routes.post('/person', savePerson)

routes.post('/createUser', createUser)

routes.get('/companybranch/all', cors(), getAllCompanyBranches)

routes.get('/status', (req, res, next) => {
  try {
    res.send({ status: 'ok' })
  } catch (error) {
    next(error)
  }
})

module.exports = routes
