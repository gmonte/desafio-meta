const userRepo = require('../../../infraestructure/repository/user')

const saveUser = async (req, res) => {
  const user = await userRepo.saveUser(req.body)
  res.json(user)
}

module.exports = {
  saveUser
}
