const userRepo = require('../../../infraestructure/repository/user')

const getUser = async (req, res) => {
  let user = null

  if (req.query.firebaseId) {
    user = await userRepo.getUserByFirebaseId(req.query.firebaseId)
  } else if (req.query.id) {
    user = await userRepo.getUser(req.query.id)
  }

  res.json(user)
}

module.exports = {
  getUser
}
