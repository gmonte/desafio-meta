const personRepo = require('../../../infraestructure/repository/person')

const savePerson = async (req, res) => {
  const person = await personRepo.savePerson(req.body)
  res.json(person)
}

module.exports = {
  savePerson
}
