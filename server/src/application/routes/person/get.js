const personRepo = require('../../../infraestructure/repository/person')

const getPerson = async (req, res) => {
  const person = await personRepo.getPerson(req.query.id)
  res.json(person)
}

module.exports = {
  getPerson
}
