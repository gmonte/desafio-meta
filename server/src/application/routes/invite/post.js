const inviteRepo = require('../../../infraestructure/repository/invite')
const userRepo = require('../../../infraestructure/repository/user')

const saveInvite = async (req, res) => {
  const { userInfo: { uid }, body } = req
  const user = await userRepo.getUserByFirebaseId(uid)
  if (user) {
    const invite = await inviteRepo.saveInvite({ ...body, userId: user.id })
    res.json(invite)
  }
  // TODO Exception error
}

module.exports = {
  saveInvite
}
