const inviteRepo = require('../../../../infraestructure/repository/invite')
const userRepo = require('../../../../infraestructure/repository/user')

const getAllInvites = async (req, res) => {
  const { query: { userId } } = req
  const invites = await inviteRepo.getAllInvites(userId)
  res.json(invites)
}

const getInvites = async (req, res) => {
  const { userInfo: { user_id: userId }, query: { type } } = req
  const user = await userRepo.getUserByFirebaseId(userId)
  let invites
  if (user) {
    const { id } = user
    invites = await inviteRepo.getInvites(id, type) || []
  }
  res.json(invites)
}

module.exports = {
  getAllInvites,
  getInvites
}
