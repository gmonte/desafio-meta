const invitationAccept = require('../../../../domain/invite/invitation-accept')

const acceptInvite = async (req, res) => {
  const invite = await invitationAccept.accept(req.body)
  res.status(200).send(invite)
}

module.exports = {
  acceptInvite
}
