const inviteRepo = require('../../../infraestructure/repository/invite')

const getInvite = async (req, res) => {
  const { query: { id } } = req
  const invite = await inviteRepo.getInvite(id)
  res.json(invite)
}

const getInviteUuid = async (req, res) => {
  const { params: { uuid } } = req
  const invite = await inviteRepo.getInviteUuid(uuid)
  res.json(invite)
}

module.exports = {
  getInvite, getInviteUuid
}
