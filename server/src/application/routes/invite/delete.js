const inviteRepo = require('../../../infraestructure/repository/invite')

const deleteInvite = async (req, res) => {
  const { query: { id } } = req
  await inviteRepo.deleteInvite(id)
  res.sendStatus(200)
}

module.exports = {
  deleteInvite
}
