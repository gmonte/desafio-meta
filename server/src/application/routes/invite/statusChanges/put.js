const inviteRepo = require('../../../../infraestructure/repository/invite')

const setStatusViwed = async (req, res) => {
  const { id } = await inviteRepo.getInviteUuid(req.query.uuid)
  const invite = await inviteRepo.saveInvite({ id, status: 'VIEWED' })
  res.status(200).send(invite)
}

const setStatusRejected = async (req, res) => {
  const { id } = await inviteRepo.getInviteUuid(req.query.uuid)
  const invite = await inviteRepo.saveInvite({ id, status: 'REJECTED', reasonRejected: req.body.reason })
  res.status(200).send(invite)
}

module.exports = {
  setStatusViwed, setStatusRejected
}
