const companyBranchRepo = require('../../../infraestructure/repository/companyBranch')

const getAllCompanyBranches = async (req, res) => {
  const { query: { id } } = req
  const companyBranches = await companyBranchRepo.getAllCompanyBranches(id)
  res.json(companyBranches)
}

module.exports = {
  getAllCompanyBranches
}
