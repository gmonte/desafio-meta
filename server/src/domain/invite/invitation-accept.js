const personRepo = require('../../infraestructure/repository/person')
const inviteRepo = require('../../infraestructure/repository/invite')
const isEmpty = require('lodash/isEmpty')

const accept = async (data) => {
  const {
    companyBranch,
    user,
    person: p,
    ...invite
  } = await inviteRepo.getInviteUuid(data.uuid)

  // se vier person, e um aceite de vemPraMeta, se nao vier, eh um aceite de conhecaAMeta
  if (!isEmpty(data.person)) {
    const person = await personRepo.getPersonByCpf(data.person.cpf)

    if (person && person.id) {
      data.person.id = person.id
    }

    const personPersited = await personRepo.savePerson(data.person)
    invite.personId = personPersited.id
  }

  invite.status = 'ACCEPTED'

  return inviteRepo.saveInvite(invite)
}

module.exports = {
  accept
}
