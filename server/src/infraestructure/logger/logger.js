const { createLogger, format, transports } = require('winston')

// Configurando Logger ## MORE INFO: https://github.com/winstonjs/winston
const logger = createLogger({
  format: format.combine(
    format.splat(),
    format.simple()
  ),
  transports: [new transports.Console()]
})

module.exports = logger
