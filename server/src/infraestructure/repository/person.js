const knex = require('../database/schema-knex')
const logger = require('../logger/logger')

const getPerson = async (id) => {
  const person = await knex.from('person').where('id', id).first()
  return person
}

const getPersonByCpf = async (cpf) => {
  const person = await knex.from('person').where('cpf', cpf).first()
  return person
}

const savePerson = async ({ id, ...person }) => {
  let idControl = id
  if (idControl) {
    await knex('person')
      .update(person)
      .where('id', idControl)
      .then((result) => {
        logger.info(`result : ${ JSON.stringify(result) }`)
      })
  } else {
    await knex('person').insert(person).then((result) => {
      logger.info(`result : ${ JSON.stringify(result) }`)
      // eslint-disable-next-line prefer-destructuring
      idControl = result[0]
    }).catch((err) => {
      logger.error(`err : ${ JSON.stringify(err) }`)
    })
  }
  return getPerson(idControl)
}

module.exports = {
  getPerson,
  savePerson,
  getPersonByCpf
}
