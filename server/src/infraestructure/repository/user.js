const knex = require('../database/schema-knex')
const logger = require('../logger/logger')

const getUserByFirebaseId = async (firabaseId) => {
  const user = await knex.from('user').where('firebaseId', firabaseId).first()
  return user
}

const getUserByEmail = async (userEmail) => {
  const user = await knex.from('user').where('mail', userEmail).first()
  return user
}

const getUser = async (id) => {
  const user = await knex.from('user').where('id', id).first()
  return user
}

const saveUser = async ({ id, ...user }) => {
  // o front envia o firebaseId na prop id
  const persistUser = { ...user, firebaseId: id }
  const userAux = await getUserByFirebaseId(id)

  if (userAux && userAux.id) {
    await knex('user')
      .update(persistUser)
      .where('firebaseId', id)
      .then((result) => { logger.info(`result : ${ JSON.stringify(result) }`) })
  } else {
    await knex('user').insert(persistUser)
  }

  return getUserByFirebaseId(id)
}

module.exports = {
  getUser,
  getUserByFirebaseId,
  saveUser,
  getUserByEmail
}
