const knex = require('../database/schema-knex')

const getAllCompanyBranches = async () => {
  const companyBranches = await knex.from('companyBranch').orderBy('name')
  return companyBranches
}

const getByid = async (id) => {
  const branch = await knex('companybranch').where('id', id).first()
  return branch
}

module.exports = { getAllCompanyBranches, getByid }
