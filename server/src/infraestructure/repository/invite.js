const knex = require('../database/schema-knex')
const moment = require('moment')
const uuid = require('uuid/v1')
const logger = require('../logger/logger')
const map = require('lodash/map')
const axios = require('axios')
const sgMail = require('@sendgrid/mail')
const isEmpty = require('lodash/isEmpty')

const getInvite = async (id) => {
  if (!id) return null
  const invite = await knex.from('invite').where('id', id).first()
  let { visitDate } = invite
  if (visitDate) visitDate = moment(visitDate, 'YYYY-MM-DD HH:mm:ss').toDate()
  const companyBranch = await knex.from('companyBranch').where('id', invite.companyBranchId || 9999).first()
  const user = await knex.from('user').where('id', invite.userId || 9999).first()
  const person = await knex.from('person').where('id', invite.personId).first()
  return {
    ...invite, visitDate, companyBranch, user, person
  }
}

const getInviteUuid = async (hashUuid) => {
  if (!hashUuid) return null

  const invite = await knex.from('invite').where('uuid', hashUuid).first()
  let { visitDate } = invite
  if (visitDate) visitDate = moment(visitDate, 'YYYY-MM-DD HH:mm:ss').toDate()
  const companyBranch = await knex.from('companyBranch').where('id', invite.companyBranchId || 9999).first()
  const user = await knex.from('user').where('id', invite.userId || 9999).first()
  const person = await knex.from('person').where('id', invite.personId).first()
  return {
    ...invite, visitDate, companyBranch, user, person
  }
}

const getAllInvites = async (userId) => {
  if (!userId) return null
  const invites = await knex.from('invite').where('userId', userId)

  const promises = map(invites, invite => Promise.all([
    asyncGetCompanyBranch(invite),
    asyncGetUser(invite),
    asyncGetPerson(invite)
  ])
    .then(([companyBranch, user, person]) => ({
      ...invite,
      companyBranch,
      user,
      person
    })))

  return Promise.all(promises)
}

const getInvites = async (userId, type) => {
  const query = knex.from('invite').whereNotNull('id')
  if (userId)query.andWhere('userId', userId)
  if (type)query.andWhere('type', type)
  const invites = await query

  const promises = map(invites, invite => Promise.all([
    asyncGetCompanyBranch(invite),
    asyncGetUser(invite),
    asyncGetPerson(invite)
  ])
    .then(([companyBranch, user, person]) => ({
      ...invite,
      companyBranch,
      user,
      person
    })))

  const response = await Promise.all(promises)
  return response
}

const saveInvite = async ({ id, internalLink, ...invite }) => {
  let { visitDate } = invite
  if (visitDate) visitDate = moment(visitDate, 'YYYY-MM-DD HH:mm:ss').toDate()

  let isNewInvite = false

  if (id) {
    await knex('invite').update(invite)
      .where('id', id)
      .then((result) => { logger.info(`result : ${ JSON.stringify(result) }`) })
  } else {
    const uuidValue = uuid()
    const dynamicLink = await createLink(uuidValue, invite.type, internalLink)
    const newInvite = { ...invite, uuid: uuidValue, dynamicLink }
    const result = await knex('invite').insert(newInvite)
    // eslint-disable-next-line prefer-destructuring
    id = result[0]
    isNewInvite = true
  }

  const persistedInvite = await getInvite(id)
  if (isNewInvite) sendEmail(persistedInvite)
  return persistedInvite
}

const sendEmail = (invite) => {

  if (isEmpty(invite.email)) {
    logger.info('Abortando envio do e-mail, pois ele nao foi informado no convite')
    return
  }

  let text = ''
  let subject = 'Meta'

  // const dateStr = moment(invite.visitDate).format('DD/MM/YYYY HH:mm')
  // <div>Venha falar conosco no seguinte horário e local:</div>
  // <div>Local: <strong>${ invite.companyBranch.address }</strong></div>
  // <div>Horário: <strong>${ dateStr }</strong><br/><br/></div>
  // <div>Convidado por: <strong>${ invite.user.displayName }</strong><br/><br/></div>  

  if (invite.type === 'vemPraMeta') {
    subject = `#vemPraMeta ${ invite.guestName }`
    text = `
    <div>
      <div>Olá <strong>${ invite.guestName }</strong>, você foi convidado a fazer parte do nosso time!<br/><br/></div>
      <div>Clique <a href='${ invite.dynamicLink }'>aqui</a> para abrir seu convite e confirmar a visita.<br/><br/></div>
      <div><strong>Aguardamos você!</strong></div>
    </div>
    `
  } else {
    subject = `#conhecaAMeta ${ invite.guestName }`
    text = `
    <div>
      <div>Olá <strong>${ invite.guestName }</strong>, você foi convidado para conhecer a Meta!<br/><br/></div>
      <div>Clique <a href='${ invite.dynamicLink }'>aqui</a> para abrir seu convite e confirmar a visita.<br/><br/></div>
      <div><strong>Aguardamos você!</strong></div>
    </div>
    `
  }

  sgMail.setApiKey(process.env.SENDGRID_API_KEY)
  const msg = {
    to: invite.email,
    from: 'naoresponda@desafiometa-recanto-pf.com.br',
    subject,
    html: text,
  }
  sgMail.send(msg, false, (err) => {
    if (err) {
      logger.error('Erro ao enviar o e-mail')
      logger.error(err)
    } else {
      logger.info(`Email enviado: ${ invite.email }`)
    }
  })
}

const deleteInvite = async (id) => {
  await knex('invite')
    .del()
    .where('id', id)
    .then((result) => { logger.info(`result : ${ JSON.stringify(result) }`) })
}

/** START - Complementary Functions  */
const createLink = async (uuidValue, type, internalLink) => {
  let typeLinkDomain = ''
  if (type === 'conhecaMeta') {
    typeLinkDomain = 'conhecaameta'
  } else if (type === 'vemPraMeta') {
    typeLinkDomain = 'vemprameta'
  }

  const body = {
    longDynamicLink: `https://${ typeLinkDomain }.page.link/?link=${ internalLink }/${ uuidValue }`
  }

  const response = await axios
    .post(`https://firebasedynamiclinks.googleapis.com/v1/shortLinks?key=${ process.env.FIREBASE_API_KEY }`, body)
    .catch((err) => {
      logger.error(`err : ${ JSON.stringify(err) }`)
    })

  return response.data.shortLink
}

const asyncGetCompanyBranch = async (item) => {
  const cb = await getCompanyBranch(item)
  return cb
}

const asyncGetUser = async (item) => {
  const user = await getUser(item)
  return user
}

const asyncGetPerson = async (item) => {
  const person = await getPerson(item)
  return person
}

const getCompanyBranch = ({ companyBranchId = 9999 }) => knex.from('companyBranch').where('id', companyBranchId).first()
const getUser = ({ userId = 9999 }) => knex.from('user').where('id', userId).first()
const getPerson = ({ personId = 9999 }) => knex.from('person').where('id', personId).first()
/** END - Complementary Functions  */

module.exports = {
  getInvite,
  getInvites,
  saveInvite,
  deleteInvite,
  getAllInvites,
  getInviteUuid
}
