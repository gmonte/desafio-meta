const admin = require('firebase-admin')

const serviceAccount = require('../../conheca-a-meta-firebase-adminsdk-6q5uu-d272ed4f5d.json')

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: 'https://conheca-a-meta.firebaseio.com'
})

const createUser = async (req, res) => {
  const {
    email,
    phoneNumber,
    password,
    firstName,
    lastName,
    photoUrl
  } = req.body

  const user = await admin.auth().createUser({
    email,
    phoneNumber,
    password,
    displayName: `${ firstName } ${ lastName }`,
    photoURL: photoUrl
  })

  return res.send(user)
}

const getAuthToken = (req, res, next) => {
  if (
    req.headers.authorization
      && req.headers.authorization.split(' ')[0] === 'Bearer'
  ) {
    // eslint-disable-next-line prefer-destructuring
    req.authToken = req.headers.authorization.split(' ')[1]
  } else {
    req.authToken = null
  }
  next()
}

const checkIfAuthenticated = (req, res, next) => {
  getAuthToken(req, res, async () => {
    try {
      const { authToken } = req
      const userInfo = await admin
        .auth()
        .verifyIdToken(authToken)
      req.authId = userInfo.uid
      req.userInfo = userInfo
      return next()
    } catch (e) {
      return res
        .status(401)
        .send({ error: 'You are not authorized to make this request' })
    }
  })
}

module.exports = { admin, createUser, checkIfAuthenticated }
