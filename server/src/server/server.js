const express = require('express')
const bodyParser = require('body-parser')
const { routes } = require('../application/application')
const logger = require('../infraestructure/logger/logger')
const knex = require('../infraestructure/database/schema-knex')
const compression = require('compression')
const cors = require('cors')
const helmet = require('helmet')

// Leitura das configs do .env
require('dotenv').config({ encoding: 'utf8' })

// Carregando o Express
const app = express()

// Configuração cors
app.use(cors())

// Registrando middleware parser de JSON application/json
app.use(bodyParser.json())

// Registrando middleware parser de application/xwww-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }))

app.use(express.static('public'))

// Configurando pasta de arquivos públicos (imgs, assets, downloads, etc)
app.use('/public', express.static('public'))

// Rotas
app.use('/', routes)

// Protegendo o express contra determinados HTTP Headers
app.use(helmet())

// Configurando Compressão
app.use(compression())

// Para prevenir erros em testes
if (require.main === module) {
  const migConfig = {
    tableName: 'knex_migrations',
    directory: `${ __dirname }../../../devops/knex/migrations`
  }

  knex.migrate
    .latest(migConfig)
    .then((result) => {
      logger.info(`Migrations OK... ${ result } ${ process.env.NODE_ENV }`)
      app.listen(process.env.PORT, (req, res) => {
        logger.info(`res: ${ JSON.stringify(res) } req: ${ JSON.stringify(req) }`)
        logger.info(`Listening on port ${ process.env.PORT } ${ process.env.NODE_ENV }`)
      })
    })
    .catch((result) => {
      logger.error(`Migrations ERROR...${ JSON.stringify(result) }`)
    })

} else {
  module.exports = app
}
