exports.up = function createTable(knex) {
  return knex.schema.createTable('person', (table) => {
    table.increments('id').primary()
    table.string('name', 100).notNullable()
    table.string('phone', 16)
    table.string('email', 100)
    table.string('urlCV', 200)
  })
}

exports.down = function deleteTable(knex) {
  return knex.schema.dropTable('person')
}
