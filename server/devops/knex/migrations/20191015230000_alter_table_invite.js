
exports.up = function alterTableInvite(knex) {
  return knex.schema.table('invite', (table) => {
    table.string('email')
  })
}

exports.down = function alterTableInvite(knex) {
  return knex.schema.table('invite', (table) => {
    table.dropColumn('email')
  })
}
