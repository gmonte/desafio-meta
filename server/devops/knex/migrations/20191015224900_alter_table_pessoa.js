exports.up = function alterTable(knex) {
  return knex.schema.table('person', (table) => {
    table.string('cpf', 14)
    table.unique(['cpf'], 'unique_1_cpf')
  })
}

exports.down = function alterTable(knex) {
  return knex.schema.table('person', (table) => {
    table.dropColumn('cpf')
  })
}
