
exports.up = function alterTableInvite(knex) {
  return knex.schema.table('invite', (table) => {
    table.string('dynamicLink', 250)
  })
}

exports.down = function alterTableInvite(knex) {
  return knex.schema.table('invite', (table) => {
    table.dropColumn('dynamicLink')
  })
}
