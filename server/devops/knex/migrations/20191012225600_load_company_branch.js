exports.up = function createTable(knex) {
  return knex('companyBranch').insert([
    {
      name: 'Miami',
      linkGoogleMaps: 'https://goo.gl/maps/1GCQfmWEdQsU4BN69',
      phone: '',
      address: '80 SW 8th Street, suite 200, Miami, Florida - Zip Code: 33130'
    },
    {
      name: 'Belo Horizonte',
      linkGoogleMaps: 'https://goo.gl/maps/UqwqkkDSQ6cqwnaT7',
      phone: '',
      address: 'Rua Paraíba, 550 - 9º andar, Bairro Funcionários - CEP: 30130-140'
    },
    {
      name: 'Curitiba',
      linkGoogleMaps: 'https://goo.gl/maps/FGn1uznHU7sCEb1LA',
      phone: '+55 (41) 2101 1300',
      address: 'Rua João Negrão, 731 - 2º andar, Bairro Rebouças - CEP: 80010-200'
    },
    {
      name: 'Rio de Janeiro',
      linkGoogleMaps: 'https://goo.gl/maps/pLGcFAsBPR6czcncA',
      phone: '+55 (21) 3983-1730',
      address: 'Av. República do Chile, 330 - 14° andar, Bairro Centro - CEP: 20031-170'
    },
    {
      name: 'Passo Fundo',
      linkGoogleMaps: 'https://goo.gl/maps/e65jCTVhFdG8UC1i8',
      phone: '+55 (54) 3622 0760',
      address: 'Avenida Presidente Vargas, 134 - sala 901, Edifício Moinhos Center, Bairro Centro - CEP: 99070-000'
    },
    {
      name: 'Centro de Excelência - Recanto Maestro',
      linkGoogleMaps: 'https://goo.gl/maps/9y1SmVMk8xAsgYbE6',
      phone: '+55 (55) 3220 0385',
      address: 'Rua Recanto Maestro, 10, Distrito de São João do Polêsine - CEP: 97230-000'
    },
    {
      name: 'São Leopoldo',
      linkGoogleMaps: 'https://goo.gl/maps/QZYSZq2WexCRBANw7',
      phone: '+55 (51) 2101 1300',
      address: 'Avenida Theodomiro Porto da Fonseca, 3101 - Prédio 8, Bairro Cristo Rei - CEP: 93022-715'
    },
    {
      name: 'São Paulo',
      linkGoogleMaps: 'https://goo.gl/maps/GyDXaB7W16xUC4QW6',
      phone: '+55 (11) 2101 1300',
      address: 'Rua Gomes de Carvalho, 1329 - 4 º Andar - Conjunto 41, Bairro Vila Olímpia - CEP: 04547-005'
    },
    {
      name: 'Joinville',
      linkGoogleMaps: 'https://goo.gl/maps/6HM5GTXtPyyo9o3q8',
      phone: '',
      address:
        'Rua Doutor João Colin, 1285 (acesso pela Rua Araranguá) - sala espanha - Bairro América - CEP: 89204-001'
    }
  ])
}

exports.down = function deleteTable(knex) {
  return knex.schema.dropTable('companyBranch')
}
