
exports.up = function alterTableInvite(knex) {
  return knex.schema.table('invite', (table) => {
    table.string('title', 280).alter()
  })
}

exports.down = function alterTableInvite(knex) {
  return knex.schema.table('invite', (table) => {
    table.string('title', 100).alter()
  })
}
