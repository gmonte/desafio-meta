
exports.up = function alterTableInvite(knex) {
  return knex.schema.table('invite', (table) => {
    table.dropColumn('place')
    table.integer('companyBranchId').unsigned()
    table.foreign('companyBranchId').references('companyBranch.id')
  })
}

exports.down = function alterTableInvite(knex) {
  return knex.schema.table('invite', (table) => {
    table.dropColumn('companyBranchId')
    table.string('place')
  })
}
