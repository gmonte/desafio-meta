exports.up = function createTable(knex) {
  return knex.schema.createTable('user', (table) => {
    table.increments('id').primary()
    table.string('name', 100).notNullable()
    table.string('login', 100).notNullable()
    table.string('password', 100).notNullable()

    table.unique(['login'], 'unique_1_login')
  })
}

exports.down = function deleteTable(knex) {
  return knex.schema.dropTable('user')
}
