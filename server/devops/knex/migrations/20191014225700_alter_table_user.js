exports.up = function alterTable(knex) {
  return knex.schema.table('user', (table) => {
    table.dropColumn('name')
    table.dropColumn('login')
    table.dropColumn('password')

    table.string('displayName', 200)
    table.string('firebaseId', 200)
    table.string('jobTitle', 200)
    table.string('mail', 100)
    table.string('mobilePhone', 30)


    table.unique(['firebaseId'], 'unique_2_firebase_id')
  })
}

exports.down = function alterTable(knex) {
  return knex.schema.table('user', (table) => {
    table.dropColumn('displayName')
    table.dropColumn('firebaseId')
    table.dropColumn('jobTitle')
    table.dropColumn('mail')
    table.dropColumn('mobilePhone')

    table.string('name', 100).notNullable()
    table.string('login', 100).notNullable()
    table.string('password', 100).notNullable()

    table.unique(['login'], 'unique_1_login')
  })
}
