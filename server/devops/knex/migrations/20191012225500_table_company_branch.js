exports.up = function createTable(knex) {
  return knex.schema.createTable('companyBranch', (table) => {
    table.increments('id').primary()
    table.string('name', 100).notNullable()
    table.string('linkGoogleMaps', 100)
    table.string('phone', 18)
    table.string('address', 200)
  })
}

exports.down = function deleteTable(knex) {
  return knex.schema.dropTable('companyBranch')
}
