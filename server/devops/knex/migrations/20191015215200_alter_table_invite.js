
exports.up = function alterTableInvite(knex) {
  return knex.schema.table('invite', (table) => {
    table.integer('personId').unsigned()
  })
}

exports.down = function alterTableInvite(knex) {
  return knex.schema.table('invite', (table) => {
    table.dropColumn('personId')
  })
}
