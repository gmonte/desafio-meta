
exports.up = function alterTableInvite(knex) {
  return knex.schema.table('invite', (table) => {
    table.string('reasonRejected', 500)
  })
}

exports.down = function alterTableInvite(knex) {
  return knex.schema.table('invite', (table) => {
    table.dropColumn('reasonRejected')
  })
}
