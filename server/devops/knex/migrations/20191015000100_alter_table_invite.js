exports.up = function alterTable(knex) {
  return knex.schema.table('invite', (table) => {
    table.string('uuid', 250)
  })
}

exports.down = function alterTable(knex) {
  return knex.schema.table('invite', (table) => {
    table.dropColumn('uuid')
  })
}
