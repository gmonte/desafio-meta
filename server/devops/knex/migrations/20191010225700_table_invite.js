exports.up = function createTable(knex) {
  return knex.schema.createTable('invite', (table) => {
    table.increments('id').primary()
    table.datetime('visitDate').notNullable()
    table.string('guestName', 100).notNullable()
    table.string('title', 100)
    table.string('type', 12)
    table.string('status', 20)
    table.string('place', 100)
    table.integer('userId').unsigned()

    table.foreign('userId').references('user.id')
  })
}

exports.down = function deleteTable(knex) {
  return knex.schema.dropTable('invite')
}
