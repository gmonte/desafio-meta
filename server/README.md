<h1 align="center">Welcome to TVGlobo MSA NodeJS Chassis 👋</h1>
<p>
  <img alt="Version" src="https://img.shields.io/badge/version-0.0.1-blue.svg?cacheSeconds=2592000" />
</p>

> Projeto Chassis utilizando NodeJS.

## Global Dependencies

```sh
  npm i -g knex mocha yo nodemon typescript
```

## Install

```sh
yarn
```

## Usage

```sh
npm run dev
```

## Run tests

```sh
yarn test
```

## Author

👤 **Giuseppe M Pereira**

## Show your support

Give a ⭐️ if this project helped you!

---

_This README was generated with ❤️ by [readme-md-generator](https://github.com/kefranabg/readme-md-generator)_
